#ifndef MUONTOOLS_IMUONTIMECOR_H
#define MUONTOOLS_IMUONTIMECOR_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/MuonTileID.h"


/** @class IMuonTimeCor IMuonTimeCor.h MuonTools/IMuonTimeCor.h
 *
 *
 *  @author Alessia Satta
 *  @date   2009-12-22
 */
struct IMuonTimeCor : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID ( IMuonTimeCor, 2, 0 );
  virtual StatusCode getCorrection(LHCb::MuonTileID tile,int & cor)=0;
  virtual StatusCode getOutCorrection(LHCb::MuonTileID tile,int & cor)=0;
  virtual StatusCode setOutCorrection(LHCb::MuonTileID tile,int  cor)=0;
  virtual StatusCode writeOutCorrection()=0;
  virtual StatusCode writeCorrection()=0;

};
#endif // MUONTOOLS_IMUONTIMECOR_H
