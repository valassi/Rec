#ifndef IMVATOOL_H_
#define IMVATOOL_H_

#include "GaudiKernel/IAlgTool.h"
#include "MuonID/CommonMuonHit.h"

#include "Event/Track.h"

static const InterfaceID IID_IMVATool("IMVATool", 1, 0);

class IMVATool : virtual public IAlgTool {
 public:
  struct MVAResponses {
      double TMVA;
      float CatBoost;
  };
  virtual bool getRunTMVA() const noexcept = 0;
  virtual bool getRunCatBoost() const noexcept = 0;
  static const InterfaceID& interfaceID() { return IID_IMVATool; }
  virtual auto calcBDT(const LHCb::Track&, const CommonConstMuonHits&) const noexcept -> MVAResponses = 0;
};

#endif  // IMVATOOL_H_
