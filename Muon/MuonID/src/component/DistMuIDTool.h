#ifndef DISTMUIDTOOL_H
#define DISTMUIDTOOL_H 1

// Include files
#include <bitset>
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Chi2MuIDTool.h"


/** @class DistMuIDTool DistMuIDTool.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2009-03-12
 */
class DistMuIDTool : public Chi2MuIDTool {
public:
  /// Standard constructor
  using Chi2MuIDTool::Chi2MuIDTool;

  StatusCode muonQuality(LHCb::Track& muTrack, double& Quality) override;

private:
  StatusCode computeDistance(const LHCb::Track& muTrack,double& dist,
                             std::bitset<4> sts_used );

  Gaudi::Property<bool> m_applyIsmuonHits {this, "ApplyIsmuonHits", false};

};
#endif // DISTMUIDTOOL_H
