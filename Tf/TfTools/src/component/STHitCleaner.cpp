// Include files

// local
#include "STHitCleaner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : STHitCleaner
//
// 2007-07-20 : Chris Jones
//-----------------------------------------------------------------------------

using namespace Tf;

// Declaration of the Tool Factory
DECLARE_COMPONENT( STHitCleaner )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STHitCleaner::STHitCleaner( const std::string& type,
                            const std::string& name,
                            const IInterface* parent )
  : base_class ( type , name , parent )
{
  declareInterface<ISTHitCleaner>(this);
  declareProperty( "MaxSectorOccupancy", m_maxBeetleOcc = 48 );
}


//=============================================================================
// initialize
//=============================================================================
StatusCode STHitCleaner::initialize ( )
{
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  info() << "Max ST sector occupancy = " << m_maxBeetleOcc << endmsg;

  return sc;
}

STHits STHitCleaner::cleanHits( const STHits::const_iterator begin,
                                const STHits::const_iterator end ) const
{
  // clean out hot beetles
  auto output = removeHotBeetles( begin, end );
  // how many hits where cleaned
  if ( msgLevel(MSG::DEBUG) )
    debug() << "Selected " << output.size() << " out of "
            << std::distance(begin,end) << " STHits" << endmsg;
  return output;
}

STHits STHitCleaner::removeHotBeetles( const STHits::const_iterator begin,
                                       const STHits::const_iterator end ) const
{
  STHits output; output.reserve( std::distance(begin,end));
  auto currentend = begin ;
  while( currentend != end) {
    
    // select hits in this sector and add up total size
    auto currentbegin = currentend ;
    const DeSTSector& sector = (*currentbegin)->sector() ;

    unsigned int occ(0) ;
    do { 
      occ += (*currentend)->cluster().pseudoSize();
      ++currentend ; 
    } while(currentend != end && &((*currentend)->sector()) == &sector ) ;
    
    // add to output container if not too hot
    if ( msgLevel(MSG::VERBOSE) )
    {
      verbose() << "Sector " << sector.id() << " has occupancy " << occ << endmsg;
    }
    if ( occ <= m_maxBeetleOcc )
    {
      output.insert( output.end(), currentbegin, currentend );
    }
    else if ( msgLevel(MSG::DEBUG) )
    {
      debug() << "DeSTSector " << sector.id()
              << " suppressed due to high occupancy = " << occ 
              << " > " << m_maxBeetleOcc << " maximum"
              << endmsg;
    }
  }
  return output;
}

//=============================================================================
