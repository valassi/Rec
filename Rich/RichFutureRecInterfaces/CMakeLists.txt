################################################################################
# Package: RichFutureRecInterfaces
################################################################################
gaudi_subdir(RichFutureRecInterfaces v1r0)

gaudi_depends_on_subdirs(Event/RecEvent
                         Rich/RichUtils)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_dictionary(RichFutureRecInterfaces
                     dict/RichFutureRecInterfacesDict.h
                     dict/RichFutureRecInterfacesDict.xml
                     LINK_LIBRARIES RecEvent RichUtils
                     OPTIONS "-U__MINGW32__ -DBOOST_DISABLE_ASSERTS")

gaudi_install_headers(RichFutureRecInterfaces)
