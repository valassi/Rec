
#pragma once

// STL
#include <string>
#include <vector>
#include <array>

// ROOT includes 
#include <TH1.h> 
#include <TF1.h>

// LHCb Kernel
#include "Kernel/RichRadiatorType.h"

namespace Rich
{
  namespace Rec
  {
    /** @class CKResolutionFitter RichRecUtils/RichCKResolutionFitter.h
     *
     *  Utility class to perform a fit to the Cherenkov resolution plots.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   2017-08-02
     */
    class CKResolutionFitter final
    {

    public:

      /// Fitter Parameters
      class FitParams final
      {

      public:  // General fitting parameters

        using RichFitTypes_t = std::array< std::vector<std::string> , 2 >;

        //                                         RICH1          RICH2
        /// Starting resolution guesses
        std::array<double,2> RichStartRes      { {  0.0017,      0.0007   } };
        /// Starting prefit deltas
        std::array<double,2> RichStartDelta    { {  0.0025,      0.00105  } };
        /// Fit Range Minimums  
        std::array<double,2> RichFitMin        { { -0.0079,     -0.00395  } };
        /// Fit range Maxmimums
        std::array<double,2> RichFitMax        { {  0.0077,      0.00365  } };
        /// Fit Type(s)
        RichFitTypes_t RichFitTypes            { { {"FreeNPol"}, {"FreeNPol"} } };

      public:  // Free polynominal background fit options

        /// Polynominal degree
        std::array<unsigned int,2> RichNPol   { {    3      ,     3      } };

      public:  // Fixed polynominal background function

        /// polynominal Background forms
        inline std::string RichFixedNPolFunc( const Rich::RadiatorType rad,
                                              const unsigned int iFirstParam ) const
        { 
          const auto s = std::to_string(iFirstParam);
          return ( Rich::Rich1Gas == rad ?
                   "(1.977+["+s+"]*x*1.701e1-x*x*2.249e3+x*x*x*9.226e3)" :
                   "(1.072+["+s+"]*x*1.618e1-x*x*8.240e3-x*x*x*3.240e5)" );
        }

      private:

        // hyperbolic fnction
        inline std::string Hyperbolic( const unsigned int iFirstParam ) const
        {
          return ( 
            // The fit parameters
            "const double A  = p["+std::to_string(iFirstParam  )+"]; "
            "const double B  = p["+std::to_string(iFirstParam+1)+"]; "
            "const double Cm = p["+std::to_string(iFirstParam+2)+"]; "
            "const double Cp = p["+std::to_string(iFirstParam+3)+"]; "
            "const double D  = p["+std::to_string(iFirstParam+4)+"]; "
            // Which C
            "const double C = ( x[0] < D ? Cm : Cp ); "
            // the function
            "const double hyperbol = ( A - ( std::sqrt( B + C*std::pow(x[0]-D,2) ) ) ); "
            );
        }

        // Gaussian function
        inline std::string Gauss( const unsigned int iFirstParam ) const
        {
          return ( 
            // The fit parameters
            "const double N     =      p["+std::to_string(iFirstParam  )+"]; "
            "const double mean  =      p["+std::to_string(iFirstParam+1)+"]; "
            "const double sigma = fabs(p["+std::to_string(iFirstParam+2)+"]); "
            // the function
            "const double gaus = ( sigma > 0 ? N * std::exp( -0.5 * std::pow((x[0]-mean)/sigma,2) ) : 0 ); "
            );
        }

      public:  // Hyperbolic functions

        //                                          RICH1      RICH2
        /// Side band sizes
        std::array<double,2> SideBandSize     { {  0.002,      0.001   } };
        /// Init values for parameter B
        std::array<double,2> HyperInitB       { {  1e8,        1e8     } };
        /// Init values for parameter D
        std::array<double,2> HyperInitD       { {  7e-4,       5e-4    } };
  
        /// lambda function for signal + background
        inline std::string GaussHyperbolFitFunc( const unsigned int iFirstParam ) const
        {
          return ( "[&](double *x, double *p) { "
                   + Gauss(iFirstParam)
                   + Hyperbolic(iFirstParam+3)
                   + "return hyperbol + gaus; }" );
        }

        /// lambda function for background only
        inline std::string HyperbolFitFunc( const unsigned int iFirstParam ) const
        {
          return ( "[&](double *x, double *p) { "
                   + Hyperbolic(iFirstParam)
                   + "return hyperbol; }" );
        }
        
      public:  // Fit Quality options

        /// Maximum abs fitted mean for OK fit
        std::array<double,2> RichMaxMean      { {  0.006,    0.003   } };
        /// Maximum abs fitted sigma for OK fit
        std::array<double,2> RichMaxSigma     { {  0.010,    0.005   } };
        /// Maximum shift error for OK fit
        std::array<double,2> MaxShiftError    { {  0.001,    0.001   } };
        /// Maximum resolution error for OK fit
        std::array<double,2> MaxSigmaError    { {  0.0002,   0.0001  } };

      };
      
      /// Class to store the result of a histogram fit
      class FitResult final
      {
      public:
        /// Overall fit status
        bool fitOK{false};
        /// CK shift
        double ckShift{0};
        /// CK shift error
        double ckShiftErr{9999};
        /// CK resolution
        double ckResolution{0};
        /// CK resolution
        double ckResolutionErr{9999};
        /// The minimum of the used fit range
        double fitMin{0};
        /// The maximum of the used fit range
        double fitMax{0};
        /// The overall fitted function
        TF1 overallFitFunc;
        /// The fitted background function
        TF1 bkgFitFunc;
      };

    public:

      /// Default Constructor
      CKResolutionFitter();

      /// Constructor with a given set of parameters
      CKResolutionFitter( const FitParams& prms ) : m_params(prms) { }

    public:

      /// Perform a fit to the given TH1 histogram
      FitResult fit( TH1 & hist, const Rich::RadiatorType rad ) const;
  
      /// Write access to the fit parameter object
      FitParams       & params()       noexcept { return m_params; }

      /// Read access to the fit parameter object
      const FitParams & params() const noexcept { return m_params; }
      
    private:

      // Run a given fit type
      FitResult fitImp( TH1 & hist,
                        const Rich::RadiatorType rad,
                        const std::string& fitType ) const;

      /// Check histogram fit result
      bool fitIsOK( const TF1& fFitF,
                    const Rich::RadiatorType rad ) const;

      /// check fit status
      template< typename RESULT >
      inline bool fitIsValid( const RESULT & fitR ) const
      {
        return ( fitR.Get() ? fitR->IsValid() : false );
      }

      /// Get the RICH array index from enum
      inline int rIndex( const Rich::RadiatorType rad ) const noexcept
      {
        return ( Rich::Rich1Gas == rad ? 0 : 1 );
      }

    private:

      /// Parameters
      FitParams m_params;

    };

  }
}
