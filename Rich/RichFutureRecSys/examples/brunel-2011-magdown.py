from Gaudi.Configuration import *
from Brunel.Configuration import *

importOptions("$BRUNELROOT/tests/options/testBrunel-defaults.py")

importOptions("$PRCONFIGOPTS/Brunel/PR-COLLISION11-Beam3500GeV-VeloClosed-MagDown.py")

importOptions("$APPCONFIGOPTS/Brunel/MonitorExpress.py")

Brunel().EvtMax=200
Brunel().PackType="MDF"
MessageSvc().countInactive=True
Brunel().OnlineMode=True

#Brunel().SkipEvents = 164
