from Gaudi.Configuration import *

importOptions("$APPCONFIGOPTS/Brunel/MC-WithTruth.py")

from Configurables import L0Conf
L0Conf().EnsureKnownTCK = False

#from PRConfig import TestFileDB
#TestFileDB.test_file_db['upgrade-baseline-FT61-digi'].run()

importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

from Configurables import Brunel, LHCbApp

LHCbApp().Simulation = True
#Brunel().Monitors = ["SC","FPE"]

Brunel().EvtMax      = -1
Brunel().PrintFreq   = 100

Brunel().OutputType = 'XDST'
Brunel().FilterTrackStates = False

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichHPD" ]
msgSvc.Format = "% F%30W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 30

