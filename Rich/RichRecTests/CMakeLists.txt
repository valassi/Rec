################################################################################
# Package: RichRecTests
################################################################################
gaudi_subdir(RichRecTests v1r0)

gaudi_depends_on_subdirs(Det/RichDet
                         GaudiKernel
                         Kernel/LHCbKernel
                         Kernel/LHCbMath
                         Kernel/VectorClass
                         Rich/RichUtils
                         Rich/RichRecUtils)

#find_package(vectorclass)
find_package(Boost)
find_package(GSL)
find_package(Eigen)
find_package(Vc)
find_package(VDT)
find_package(ROOT COMPONENTS MathCore GenVector )

#message(STATUS "Using Vc ${Vc_INCLUDE_DIR} ${Vc_LIB_DIR}")
#message(STATUS "Vc DEFINITIONS ${Vc_DEFINITIONS}")
#message(STATUS "Vc COMPILE flags ${Vc_COMPILE_FLAGS}")
#message(STATUS "Vc ARCHITECTURE flags ${Vc_ARCHITECTURE_FLAGS}")

# Determine if AVX2 is supported
set(AVX2_BUILD_FLAGS    " -Wno-ignored-attributes -mavx ")
set(AVX2FMA_BUILD_FLAGS " -Wno-ignored-attributes -mavx ")
exec_program(${CMAKE_CXX_COMPILER} ARGS -print-prog-name=as OUTPUT_VARIABLE _as)
if(NOT _as)
  message(ERROR "Could not find the 'as' assembler...")
else()
  exec_program(${_as} ARGS --version OUTPUT_VARIABLE _as_version)
  string(REGEX REPLACE "\\([^\\)]*\\)" "" _as_version "${_as_version}")
  string(REGEX MATCH "[1-9]\\.[0-9]+(\\.[0-9]+)?" _as_version "${_as_version}")
  if(_as_version VERSION_LESS "2.21.0")
     message(WARNING "binutils is too old to support AVX2+FMA... Falling back to AVX only.")
  else()
     set(AVX2_BUILD_FLAGS    " -Wno-ignored-attributes -mavx2 ")
     set(AVX2FMA_BUILD_FLAGS " -Wno-ignored-attributes -mavx2 -mfma ")
  endif()
endif()
message(STATUS "AVX2 build flags = " ${AVX2_BUILD_FLAGS})

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${EIGEN_INCLUDE_DIRS} ${Vc_INCLUDE_DIR})

if(GAUDI_BUILD_TESTS)



  gaudi_add_executable(RichMirrorCompareTestSSE4
                       src/MirrorCompare/main_sse4.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichMirrorCompareTestSSE4 "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/MirrorCompare/main_sse4.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -msse4.2 " )  

  gaudi_add_executable(RichMirrorCompareTestAVX
                       src/MirrorCompare/main_avx.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichMirrorCompareTestAVX "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/MirrorCompare/main_avx.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -mavx " )  

  gaudi_add_executable(RichMirrorCompareTestAVX2
                       src/MirrorCompare/main_avx2.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichMirrorCompareTestAVX2 "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/MirrorCompare/main_avx2.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2_BUILD_FLAGS} ) 

  gaudi_add_executable(RichMirrorCompareTestAVX2FMA
                       src/MirrorCompare/main_avx2fma.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichMirrorCompareTestAVX2FMA "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/MirrorCompare/main_avx2fma.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2FMA_BUILD_FLAGS} )    



  gaudi_add_executable(RichSIMDMathsTestSSE4
                       src/SIMDMaths/main_sse4.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichSIMDMathsTestSSE4 "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/SIMDMaths/main_sse4.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -msse4.2 " )  

  gaudi_add_executable(RichSIMDMathsTestAVX
                       src/SIMDMaths/main_avx.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichSIMDMathsTestAVX "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/SIMDMaths/main_avx.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -mavx " )  

  gaudi_add_executable(RichSIMDMathsTestAVX2
                       src/SIMDMaths/main_avx2.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichSIMDMathsTestAVX2 "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/SIMDMaths/main_avx2.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2_BUILD_FLAGS} ) 

  gaudi_add_executable(RichSIMDMathsTestAVX2FMA
                       src/SIMDMaths/main_avx2fma.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichSIMDMathsTestAVX2FMA "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/SIMDMaths/main_avx2fma.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2FMA_BUILD_FLAGS} )    



  gaudi_add_executable(RichPhotonRecoTestSSE4
                       src/PhotonReco/main_sse4.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichPhotonRecoTestSSE4 -lrt )
  set_property(SOURCE src/PhotonReco/main_sse4.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -msse4.2 " )  

  gaudi_add_executable(RichPhotonRecoTestAVX
                       src/PhotonReco/main_avx.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichPhotonRecoTestAVX -lrt )
  set_property(SOURCE src/PhotonReco/main_avx.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -mavx " )  

  gaudi_add_executable(RichPhotonRecoTestAVX2
                       src/PhotonReco/main_avx2.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichPhotonRecoTestAVX2 -lrt )
  set_property(SOURCE src/PhotonReco/main_avx2.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2_BUILD_FLAGS} ) 

  gaudi_add_executable(RichPhotonRecoTestAVX2FMA
                       src/PhotonReco/main_avx2fma.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichPhotonRecoTestAVX2FMA -lrt )
  set_property(SOURCE src/PhotonReco/main_avx2fma.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2FMA_BUILD_FLAGS} )    




  gaudi_add_executable(RichRayTracingTestSSE4
                       src/RayTracing/main_sse4.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichRayTracingTestSSE4 -lrt )
  set_property(SOURCE src/RayTracing/main_sse4.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-ignored-attributes -msse4.2 " )

  gaudi_add_executable(RichRayTracingTestAVX
                       src/RayTracing/main_avx.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichRayTracingTestAVX -lrt )
  set_property(SOURCE src/RayTracing/main_avx.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-ignored-attributes -mavx " )

  gaudi_add_executable(RichRayTracingTestAVX2
                       src/RayTracing/main_avx2.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichRayTracingTestAVX2 -lrt )
  set_property(SOURCE src/RayTracing/main_avx2.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2_BUILD_FLAGS} )

  gaudi_add_executable(RichRayTracingTestAVX2FMA
                       src/RayTracing/main_avx2fma.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VectorClass VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel VectorClassLib RichUtils )
  target_link_libraries( RichRayTracingTestAVX2FMA -lrt )
  set_property(SOURCE src/RayTracing/main_avx2fma.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2FMA_BUILD_FLAGS} )



endif()
