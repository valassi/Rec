
#include <tuple>
#include <vector>
#include <memory>

#include "GlobalPID.C"

void RichKaonIDCompareFiles()
{

  // make a pid object
  auto pid = std::make_unique<GlobalPID>();

  // Default Config Object
  GlobalPID::Configuration defaultConfig;

  const Long64_t nTracks = 1e6;

  const double GeV(1000);

  // Histo range
  defaultConfig.useFixedGraphRange = true;
  //defaultConfig.minGraphX = 93;
  //defaultConfig.maxGraphX = 97;
  //defaultConfig.minGraphY = 3;
  //defaultConfig.maxGraphY = 8;
  defaultConfig.minGraphX = 88;
  defaultConfig.maxGraphX = 100;
  defaultConfig.minGraphY = 1;
  defaultConfig.maxGraphY = 20;
  // Stepping options
  defaultConfig.maxCut      = 35;
  defaultConfig.nSteps      = 100;
  defaultConfig.minMisIDeff = 1.0;
  // Momentum range
  defaultConfig.minP      = 3   * GeV;
  defaultConfig.maxP      = 100 * GeV;
  defaultConfig.minPt     = 0.5 * GeV;
  defaultConfig.maxPt     = 100 * GeV;
  // track selection
  defaultConfig.trackType = GlobalPID::Long;
  //defaultConfig.trackType = GlobalPID::Upstream;
  //defaultConfig.trackType = GlobalPID::Downstream;
  // detector selection
  defaultConfig.mustHaveAnyRICH = true;
  // Plot Type
  defaultConfig.title     = "RICH Kaon ID";
  defaultConfig.idType    = GlobalPID::Kaon;
  defaultConfig.misidType = GlobalPID::Pion;
  defaultConfig.var1      = GlobalPID::richDLLk;
  defaultConfig.var2      = GlobalPID::richDLLpi;

  //const std::string dir = "/home/chris/LHCb/future";
  const std::string dir = "/usera/jonesc/LHCbCMake/Feature/Brunel/output/Upgrade";

  typedef std::vector< std::tuple<std::string,std::string,Color_t> > PlotData;

  // colours...
  // kBlack kRed-6 kBlue+1 kGreen+2 kYellow+3 kRed+1 kMagenta+2 kCyan+2

  const PlotData plotdata = 
    {
      std::make_tuple ( dir+"/Saved/Master.protos.tuple.root",  "Master", kBlack  ),
      std::make_tuple ( dir+"/Saved/NormalNSigma.protos.tuple.root", "Feature Nominal #sigma",  kRed-6  ),
      std::make_tuple ( dir+"/Saved/TightNSigma.protos.tuple.root", "Feature Tight #sigma",  kBlue+1 ),
      std::make_tuple ( dir+"/Saved/FastGlobal.protos.tuple.root", "Feature FastGlobal",  kYellow+3 )
    };

  unsigned int iPlot = 0;
  for ( const auto& pd : plotdata )
  {
    // ROOT file
    const auto & fname = std::get<0>(pd);
    // title
    const auto & title = std::get<1>(pd);
    // colour
    const auto & color = std::get<2>(pd);
    pid->loadTTree( fname );
    pid->config             = defaultConfig;
    pid->config.subtitle    = title;
    pid->config.superImpose = ( iPlot != 0 );
    pid->config.color       = color;
    // create the plot
    pid->makeCurve(nTracks);
    ++iPlot;
  }
  
  // save the figures
  pid->saveFigures();
  
}
