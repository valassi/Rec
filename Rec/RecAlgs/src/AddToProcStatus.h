#ifndef ADDTOPROCSTATUS_H
#define ADDTOPROCSTATUS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"


/** @class AddToProcStatus AddToProcStatus.h
 *
 *  Add an entry to ProcStatus if some sequeence failed
 *
 *  @author Patrick Koppenburg
 *  @date   2011-06-15
 */
class AddToProcStatus final : public GaudiAlgorithm
{
public:

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:

  Gaudi::Property<std::string> m_subsystem { this, "Subsystem", "", "Subsystem that has aborted"};///< subsystem
  Gaudi::Property<std::string> m_reason { this, "Reason", "", "Reason"};                          ///< Reason for error
  Gaudi::Property<int> m_status { this, "Status", -3, "Status Code"};                     ///< return code
  Gaudi::Property<bool> m_abort { this, "Abort", true, "Abort Event?"};                    ///< should processing be aborted?

};

#endif // ADDTOPROCSTATUS_H
