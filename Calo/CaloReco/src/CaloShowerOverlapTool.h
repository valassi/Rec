#ifndef CALOSHOWEROVERLAPTOOL_H
#define CALOSHOWEROVERLAPTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloCluster.h"
#include "CaloCorrectionBase.h"
#include "CaloInterfaces/ICaloHypoTool.h"
#include "CaloInterfaces/ICaloShowerOverlapTool.h"            // Interface
#include "CaloInterfaces/ICounterLevel.h"


/** @class CaloShowerOverlapTool CaloShowerOverlapTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */
class CaloShowerOverlapTool : public GaudiTool, virtual public ICaloShowerOverlapTool {
public:
  /// Standard constructor
  CaloShowerOverlapTool( const std::string& type,
                         const std::string& name,
                         const IInterface* parent);

  StatusCode initialize() override;

  StatusCode setProfile(std::string) override;
  void process(const LHCb::CaloCluster* c1, const LHCb::CaloCluster* c2,
               int spd=0, int niter=10,bool propagateInitialWeights=false) override;

protected:
  void storeInitialWeights(const LHCb::CaloCluster* cl1,const LHCb::CaloCluster* cl2);
  double getInitialWeight(const LHCb::CaloCellID id);
  double fraction(LHCb::CaloCluster* c, LHCb::CaloDigit* d,int flag);
  void subtract(LHCb::CaloCluster* c1,LHCb::CaloCluster* c2,bool propagateInitialWeights);
  double showerFraction(double d3d,unsigned int area,int spd);
  void evaluate(LHCb::CaloCluster* c,bool hypoCorrection = true);
private:
  int m_a1 = 0;
  int m_a2 = 0;
  int m_s1 = 0;
  int m_s2 = 0;
  Gaudi::Property<std::string> m_detLoc {this, "Detector", DeCalorimeterLocation::Ecal};
  Gaudi::Property<std::string> m_pcond {this, "Profile", "Conditions/Reco/Calo/PhotonShowerProfile"};
  std::string m_type;
  const DeCalorimeter* m_det = nullptr;
  ICaloHypoTool*      m_stool = nullptr;
  ICaloHypoTool*      m_ltool = nullptr;
  CaloCorrectionBase* m_shape = nullptr;
  std::map<const LHCb::CaloCellID,double> m_weights;
  Gaudi::Property<bool> m_verbose {this, "Verbose", false};
  Gaudi::Property<unsigned int> m_minSize {this, "ClusterMinSize", 2};
  ICounterLevel* counterStat = nullptr;
};
#endif // CALOSHOWEROVERLAPTOOL_H
