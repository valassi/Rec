// $Id: CaloGetterInit.h,v 1.1 2009-04-17 11:44:53 odescham Exp $
#ifndef CALOGETTERINIT_H 
#define CALOGETTERINIT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
//from LHCb
#include "CaloInterfaces/ICaloGetterTool.h"

/** @class CaloGetterInit CaloGetterInit.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2009-04-17
 */
class CaloGetterInit : public GaudiAlgorithm {
public: 
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

private:
  ICaloGetterTool* m_getter = nullptr;
  Gaudi::Property<std::string> m_name {this, "ToolName", "CaloGetter"};
};
#endif // CALOGETTERINIT_H
