// Include files
#include "Relations/IRelation.h"
#include "Relations/RelationWeighted2D.h"
#include "Event/CaloCluster.h"
#include "CaloTrackMatchAlg.h"


// ============================================================================
/** @class PhotonMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// ============================================================================

class PhotonMatchAlg final: public CaloTrackMatchAlg {
 public:
  StatusCode execute() override;

  PhotonMatchAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloTrackMatchAlg(name, pSvc) {
    using namespace LHCb::CaloAlgUtils;
    Inputs inputs = Inputs(1, CaloClusterLocation("Ecal", context()));
    _setProperty("Calos", Gaudi::Utils::toString(inputs));
    _setProperty("Output", CaloIdLocation("ClusterMatch", context()));
    _setProperty("Filter", CaloIdLocation("InEcal", context()));
    _setProperty("Tool", "CaloPhotonMatch/PhotonMatch");
    _setProperty("Threshold", "1000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( PhotonMatchAlg )

// ============================================================================
// Standard execution of the algorithm
// ============================================================================

StatusCode PhotonMatchAlg::execute() {
  Assert(!m_tracks.empty(), "No Input tracks");
  Assert(!m_calos.empty(), "No Input Calos");

  using Table = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
  static_assert(std::is_base_of<LHCb::Calo2Track::IClusTrTable2D, Table>::value,
                "Table must inherit from IClusTrTable2D");

  // create the relation table and register it in TES
  Table* table = new Table(50 * 100);
  put(table, m_output);

  // perform the actual jobs
  StatusCode sc = doTheJob<LHCb::CaloCluster,Table>( table ) ;
  if(counterStat->isQuiet())counter (Gaudi::Utils::toString( m_tracks.value() )+ "->"+Gaudi::Utils::toString( m_calos.value() )
                                     + "=>" + m_output  ) += table->i_relations().size() ;
  return sc;
}
