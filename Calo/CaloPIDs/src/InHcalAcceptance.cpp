// Include files
#include "InCaloAcceptance.h"

// ============================================================================
/** @class InHcalAcceptance
 *  The precofigured instance of InCaloAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InHcalAcceptance final : InCaloAcceptance {
  /// standard constructor
  InHcalAcceptance(const std::string& type, const std::string& name,
                   const IInterface* parent)
      : InCaloAcceptance(type, name, parent) {
    _setProperty("Calorimeter", DeCalorimeterLocation::Hcal);
    _setProperty("UseFiducial", "true");
    _setProperty("Tolerance", "10");  /// 10 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InHcalAcceptance() = delete;
  InHcalAcceptance(const InHcalAcceptance&) = delete;
  InHcalAcceptance& operator=(const InHcalAcceptance&) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InHcalAcceptance )

// ============================================================================
