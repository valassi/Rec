// Include files
#include "InCaloAcceptanceAlg.h"

// ============================================================================
/** @class InHcalAcceptanceAlg InHcalAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InHcalAcceptanceAlg final : InCaloAcceptanceAlg {
  /// Standard constructor
  InHcalAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloAcceptanceAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;

    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloIdLocation("InHcal", context()));

    _setProperty("Tool", "InHcalAcceptance/InHcal");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( InHcalAcceptanceAlg )

// ============================================================================
