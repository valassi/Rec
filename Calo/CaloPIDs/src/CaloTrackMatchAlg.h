#ifndef CALOTRACKMATCHALG_H
#define CALOTRACKMATCHALG_H 1

// Include files
#include "CaloInterfaces/ICaloTrackMatch.h"
#include "CaloTrackAlg.h"
#include "CaloUtils/Calo2Track.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "Relations/IRelation.h"
#include "Relations/IsConvertible.h"
#include "Relations/RelationWeighted2D.h"
#include "ToVector.h"

// ============================================================================
/** @class CaloTrackMatchAlg CaloTrackMatchAlg.h
 *
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-16
 */
class CaloTrackMatchAlg : public CaloTrackAlg {
 public:
  /// C++11 non-copyable idiom
  CaloTrackMatchAlg() = delete;
  CaloTrackMatchAlg(const CaloTrackMatchAlg&) = delete;
  CaloTrackMatchAlg& operator=(const CaloTrackMatchAlg&) = delete;

  StatusCode initialize() override;

 protected:
  CaloTrackMatchAlg(const std::string& name, ISvcLocator* pSvc);

  /// virtual protected destructor
  virtual ~CaloTrackMatchAlg(){};

  typedef std::vector<std::string> Inputs;

  // perform the actual jobs!
  template <class TYPE, class TABLE>
  inline StatusCode doTheJob(TABLE* table) const;

  mutable ToolHandle<ICaloTrackMatch> m_matcher;  // the tool for matching

protected:

  Gaudi::Property<Inputs> m_tracks    
    {this, "Tracks", {}, "input data (tracks)"};

  Gaudi::Property<Inputs> m_calos     
    {this, "Calos", {}, "input data (calorimeter objects)"};

  Gaudi::Property<std::string> m_output    
    {this, "Output", "", "output data (relation table)"};

  Gaudi::Property<std::string> m_filter    
    {this, "Filter", "", " filter"};

  Gaudi::Property<float> m_threshold 
    {this, "Threshold", 10000., "threshold"};

};

// ============================================================================

namespace {
inline const LHCb::CaloPosition* position(const LHCb::CaloCluster* c) {
  return &c->position();
}

inline const LHCb::CaloPosition* position(const LHCb::CaloHypo* c) {
  return c->position();
}

template <class TABLE>
inline void i_push(TABLE* table, const LHCb::Track* track,
                   const LHCb::CaloCluster* cluster, const double weight) {
  table->i_push(cluster, track, (float)weight);  // NB: i_push here
}
template <class TABLE>
inline void i_push(TABLE* table, const LHCb::Track* track,
                   const LHCb::CaloHypo* hypo, const double weight) {
  table->i_push(track, hypo, (float)weight);  // NB: i_push here
}
}

// ============================================================================

template <class TYPE, class TABLE>
inline StatusCode CaloTrackMatchAlg::doTheJob(TABLE* table) const {
  using Filter = LHCb::Calo2Track::ITrAccTable;
  using Track = LHCb::Track;
  using Tracks = LHCb::Track::Container;
  using TRACKS = LHCb::Track::ConstVector;

  typedef TYPE Calo;
  typedef typename TYPE::Container Calos;

  // get all tracks locally
  TRACKS tracks;
  tracks.reserve(100);

  // use the filter?
  Filter* filter = nullptr;
  if (!m_filter.empty()) {
    filter = get<Filter>(m_filter);
  }

  // loop over the tracks containers
  for (const auto& i : m_tracks) {
    const auto tracks2 = getIfExists<Tracks>(i);
    if (!tracks2) {
      if (msgLevel(MSG::DEBUG))
        debug() << " Container " << i << " has not been found " << endmsg;
      continue;
    }
    if (msgLevel(MSG::DEBUG))
      debug() << " Found " << tracks2->size() << "tracks in " << i << endmsg;
    // loop over all tracks in container
    for (const auto& track : *tracks2) {
      // skip track?
      if (!use(track)) {
        continue;
      }
      // use filter?
      if (filter) {
        const auto r = filter->relations(track);
        if (r.empty() || !r.front().to()) {
          continue;
        }
      }
      // collect good tracks
      tracks.push_back(track);
    }  // end of loop over tracks
  }    // end of loop over track containers

  const size_t nTracks = tracks.size();
  if (0 == nTracks) {
    if (msgLevel(MSG::DEBUG))
      debug() << "No good tracks have been selected" << endmsg;
  }

  // Counters
  size_t nCalos = 0;
  size_t nOverflow = 0;
  // loop over cluster containers
  for (const auto calo_path : m_calos) {
    const auto calos = getIfExists<Calos>(calo_path);
    if (!calos) {
      if (msgLevel(MSG::DEBUG))
        debug() << " Container " << calo_path << " has not been found "
                << endmsg;
      continue;
    }
    // loop over all calos
    for (auto icalo = calos->begin(); calos->end() != icalo; ++icalo) {
      const Calo* calo = *icalo;
      const auto caloPos = position(calo);
      if (calo == nullptr || caloPos == nullptr) {
        continue;
      }

      // overall number of calos
      ++nCalos;

      // loop over all good tracks
      for (const auto track : tracks) {
        // use the tool
        double chi2 = 0;
        StatusCode sc = m_matcher->match(caloPos, track, chi2);
        if (sc.isFailure()) {
          if (msgLevel(MSG::DEBUG))
            debug() << "Failure from Tool::match, skip" << endmsg;
          if (counterStat->isQuiet())
            counter("# failure from Tool::match") += 1;
          continue;
        }
        if (m_threshold < chi2) {
          ++nOverflow;
          continue;
        }
        // FINALLY: fill the relation table!
        i_push(table, track, calo, chi2);  // NB: i_push
      }
      //
    }  // end of loop over clusters
  }    // end of loop over cluster containters ;
  // MANDATORY: i_sort after i_push
  table->i_sort();  // NB: i_sort

  // a bit of statistics
  if (statPrint() || msgLevel(MSG::DEBUG)) {
    for (int t = 0; t <= LHCb::Track::Types::UT; ++t) {
      int c =
          std::count_if(tracks.begin(), tracks.end(),
                        [t](const auto tr) { return tr->type() == t; });
      if (c == 0) continue;
      std::ostringstream s("");
      s << (LHCb::Track::Types)t;
      if (counterStat->isVerbose()) counter("#tracks[" + s.str() + "]") += c;
    }
    if (counterStat->isQuiet()) {
      const auto links = table->i_relations();
      counter("#links") += links.size();
      if (counterStat->isVerbose()) {
        counter("#tracks") += nTracks;
        counter("#calos") += nCalos;
        counter("#overflow") += nOverflow;
        StatEntity& weight = counter("#chi2");
        for (const auto& link: links){
          weight += link.weight();
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}

// ============================================================================
#endif  // CALOTRACKMATCHALG_H
// ============================================================================
