// Include files
#include "CaloTrackAlg.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/Track.h"

// ============================================================================
/** @file
 *
 *  Implementation file for class CaloCluster2TrackAlg
 *  @see CaloCluster2TrackAlg
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 02/11/2001
 */
// ============================================================================

CaloTrackAlg::CaloTrackAlg(const std::string& name, ISvcLocator* svcloc)
    : GaudiAlgorithm(name, svcloc) {
  //
  _setProperty("CheckTracks", "true");
  std::vector<int> stat = {LHCb::Track::FitStatus::Fitted};
  if (LHCb::CaloAlgUtils::hltContext(context()))
    stat.push_back(LHCb::Track::FitStatus::FitStatusUnknown);
  setProperty("AcceptedFitStatus", stat).ignore();
}

// =============================================================================
// standard algorithm initialization
// =============================================================================

StatusCode CaloTrackAlg::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if (sc.isFailure()) {
    return sc;
  }
  // Retrieve tools
  sc = counterStat.retrieve();
  if (sc.isFailure()) {
    return sc;
  }
  //
  if (propsPrint() || msgLevel(MSG::DEBUG) || m_use.check()) {
    info() << m_use << endmsg;
  };
  //
  return StatusCode::SUCCESS;
}

// =============================================================================

void CaloTrackAlg::_setProperty(const std::string& p, const std::string& v) {
  StatusCode sc = setProperty(p, v);
  if (!sc.isSuccess()) {
    warning() << " setting Property " << p << " to " << v << " FAILED"
              << endmsg;
  }
}
