// Includes 
#include "CaloMoniAlg.h"

//------------------------------------------------------------------------------
// Implementation file for class : CaloMoniAlg
//
// 2008-09-03 : Olivier Deschamps
//------------------------------------------------------------------------------

//==============================================================================
// Standard constructor, initializes variables
//==============================================================================

CaloMoniAlg::CaloMoniAlg( const std::string& name, ISvcLocator* pSvcLocator )
: Calo2Dview( name, pSvcLocator )
{
  // Areas
  m_mcount.reserve(m_nAreas);
  m_scount.reserve(2);

  //set default detectorName
  m_detData = LHCb::CaloAlgUtils::CaloNameFromAlg( name );
  if ( m_detData == "Prs" ) 
    m_energyMax = 300.* Gaudi::Units::MeV;
  if ( m_detData == "Spd" ) 
    m_energyMax = 10.* Gaudi::Units::MeV;    
}

//==============================================================================
// Initialization
//==============================================================================

StatusCode CaloMoniAlg::initialize() {
  StatusCode sc = Calo2Dview::initialize();
  if ( sc.isFailure() ) return sc;
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Initialize" << endmsg;

  m_counterStat.retrieve().ignore();
  if("" == histoTopDir()) setHistoTopDir("CaloMoniDst/");

  if( m_split && m_splitSides ){
    warning() << "Cannot split simultaneously the calo sides and areas, so far - Area splitting wins" << endmsg;
    m_splitSides=false;
  }  
  return StatusCode::SUCCESS;
}

//==============================================================================

void CaloMoniAlg::initCounters() const {
  m_count = 0;
  for(unsigned int i = 0; i != m_nAreas ; ++i){
    m_mcount[i]=0;
  }
  for(unsigned int i = 0; i <2 ; ++i){
    m_scount[i]=0;
  }
}

//==============================================================================

void CaloMoniAlg::count(LHCb::CaloCellID id) const {
  m_count++;
  if( !(id == LHCb::CaloCellID()) ){
    int area = id.area();
    m_mcount[area]++;
    int col = id.col();
    int side = 1;
    if( CaloCellCode::CaloNameFromNum(id.calo()) == "Hcal"){
      if(col<16)side = 0;
    }
    else if(col<32){
      side = 0;
    }
    m_scount[side]++;
  }
}

//==============================================================================

void CaloMoniAlg::fillCounters(std::string unit) const {
  fill(m_h1[unit], m_count , 1);

  // Source monitor. Fetch from "Input" field if existed.
  // This required consistent naming across its functional children.
  std::string cname{"Monitor"};
  if(hasProperty("Input"))
    cname += " "+getProperty("Input").toString();
  if( m_counterStat->isQuiet()) counter(cname) += m_count;
  
  // Split monitor
  if( m_splitSides ){
    for(unsigned int i = 0;i < 2 ;++i){
      std::string side = (i==0) ? "C-side" : "A-side";
      if( m_scount[i] == 0 ) continue;
      GaudiAlg::HistoID id(side + "/"+unit);
      fill(m_h1[id], m_scount[i] , 1);
      if( m_counterStat->isQuiet()) counter("Monitor (" + side +")"  ) += m_scount[i];
    }    
  } else if( m_split ){
    for(unsigned int i = 0;i != m_nAreas;++i){
      //std::string area = CaloCellCode::CaloAreaFromNum( CaloCellCode::CaloNumFromName( m_detData ), i );
      std::string area = CaloCellCode::caloArea ( CaloCellCode::caloNum ( m_detData ), i );
      if( !validArea( area )||  m_mcount[i] == 0) continue;
      GaudiAlg::HistoID id(area + "/"+unit);
      fill(m_h1[id], m_mcount[i] , 1);
      if( m_counterStat->isQuiet()) counter("Monitored (" + area +")"  ) += m_mcount[i];
    }    
  }
}

//==============================================================================
// BOOKINGS
//==============================================================================

void CaloMoniAlg::hBook1( const std::string hid,
                          const std::string titl,
                          const double low,
                          const double high,
                          const unsigned long bins ){ 
  if(!doHisto(hid)) return;

  if( m_splitSides ){
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
      debug() << "Booking histogram1D per calo side" << endmsg;
    for(unsigned int i = 0;i <2 ;++i){
      std::string side = (i==0) ? "C-side" : "A-side";
      GaudiAlg::HistoID id(side + "/" + hid);
      std::string tit = titl + " (" + side + ")";
      m_h1[id] = book1D( id, tit, low, high, bins );
    }      
  }
  else if(m_split){
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
      debug() << "Booking histogram1D per calo area" << endmsg;
    for(unsigned int i = 0;i != m_nAreas;++i){
      // std::string area = CaloCellCode::CaloAreaFromNum( CaloCellCode::CaloNumFromName( m_detData ), i );
      std::string area = CaloCellCode::caloArea ( CaloCellCode::caloNum( m_detData ), i );
      if( !validArea( area )) continue;
      GaudiAlg::HistoID id(area + "/" + hid);
      std::string tit = titl + " (" + area + ")";
      m_h1[id] = book1D( id, tit, low, high, bins );
    }
  }
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
    debug() << "Booking histogram1D for whole calo" << endmsg;
  m_h1[hid] = book1D( hid, titl, low, high, bins );
}

//==============================================================================

void CaloMoniAlg::h1binLabel( const std::string hid, int bin, std::string label ) const {
  if(!doHisto(hid)) return;

  if( m_splitSides ){
    for(unsigned int i = 0;i <2 ;++i){
      std::string side = (i==0) ? "C-side" : "A-side";
      GaudiAlg::HistoID id(side + "/" + hid);
      const auto th = Gaudi::Utils::Aida2ROOT::aida2root( m_h1[id] );
      th->GetXaxis()->SetBinLabel( bin  , label.c_str() );
    }      
  }
  else if(m_split){
    for(unsigned int i = 0;i != m_nAreas;++i){
      // std::string area = CaloCellCode::CaloAreaFromNum( CaloCellCode::CaloNumFromName( m_detData ), i );
      std::string area = CaloCellCode::caloArea ( CaloCellCode::caloNum( m_detData ), i );
      if( !validArea( area )) continue;
      GaudiAlg::HistoID id(area + "/" + hid);
      const auto th = Gaudi::Utils::Aida2ROOT::aida2root( m_h1[id] );
      th->GetXaxis()->SetBinLabel( bin  , label.c_str() );
    }
  }
  const auto th = Gaudi::Utils::Aida2ROOT::aida2root( m_h1[hid] );
  th->GetXaxis()->SetBinLabel( bin, label.c_str() );
}

//==============================================================================

void CaloMoniAlg::hBook2( const std::string hid, const std::string titl,
                          const double lowx, const double highx, const unsigned long binsx,
                          const double lowy, const double highy, const unsigned long binsy ){ 
  if(!doHisto(hid)) return;
  if( m_splitSides ){
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
      debug() << "Booking histogram2D per calo side" << endmsg;
    for(unsigned int i = 0;i <2 ;++i){
      std::string side = (i==0) ? "C-side" : "A-side";
      GaudiAlg::HistoID id(side + "/" + hid);
      std::string tit = titl + " (" + side + ")";
      m_h2[id] = book2D( id, tit, lowx, highx, binsx, lowy, highy, binsy );
    }
  }
  else if( m_split ){
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
      debug() << "Booking histogram2D per calo region" << endmsg;
    for(unsigned int i = 0;i != m_nAreas;++i){
      std::string area = CaloCellCode::caloArea ( CaloCellCode::caloNum( m_detData ), i );
      if( !validArea( area )) continue;
      GaudiAlg::HistoID id(area + "/" + hid);
      std::string tit = titl + " (" + area + ")";
      m_h2[id] = book2D( id, tit, lowx, highx, binsx, lowy, highy, binsy );
    }
  }
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
    debug() << "Booking histogram2D for whole calo" << endmsg;
  m_h2[hid] = book2D( hid, titl, lowx, highx, binsx, lowy, highy, binsy );
}

//==============================================================================
// FILLING
//==============================================================================

void CaloMoniAlg::hFill1(std::string hid, double value, double w ) const { 
  if(!doHisto(hid)) return;
  const auto h  = m_h1[hid];
  if( h == nullptr ) return;
  double bins = (double) h->axis().bins();
  double step = h->axis().upperEdge() - h->axis().lowerEdge();
  step = ( bins == 0) ? 0 : step/bins/2.;
  if(m_sat){
    if( value < h->axis().lowerEdge() )value = h->axis().lowerEdge()+step;
    if( value > h->axis().upperEdge() )value = h->axis().upperEdge()-step;
  }
  fill(h,value,w);
}

void CaloMoniAlg::hFill2(std::string hid, double x, double y, double w ) const { 
  if(!doHisto(hid)) return;
  const auto h = m_h2[hid];
  if( h == nullptr ) return;
  double xbins = (double) h->xAxis().bins();
  double xstep = h->xAxis().upperEdge() - h->xAxis().lowerEdge();
  xstep = ( xbins == 0) ? 0 : xstep/xbins/2.;
  double ybins = (double) h->yAxis().bins();
  double ystep = h->yAxis().upperEdge() - h->yAxis().lowerEdge();
  ystep = ( ybins == 0) ? 0 : ystep/ybins/2.;

  if(m_sat2D){
    if( x < h->xAxis().lowerEdge() )x = h->xAxis().lowerEdge()+xstep;
    if( x > h->xAxis().upperEdge() )x = h->xAxis().upperEdge()-xstep;
    if( y < h->yAxis().lowerEdge() )y = h->yAxis().lowerEdge()+ystep;
    if( y > h->yAxis().upperEdge() )y = h->yAxis().upperEdge()-ystep;
  }
  fill(h,x,y,w); 
}

void CaloMoniAlg::hFill1(LHCb::CaloCellID cellID , std::string hid, double value, double w ) const{ 
  if(!doHisto(hid)) return;
  const auto h = m_h1[hid];
  if( h == nullptr ) return;
  double bins = (double) h->axis().bins();
  double step = h->axis().upperEdge() - h->axis().lowerEdge();
  step = ( bins == 0) ? 0 : step/bins/2.;
  if(m_sat){
    if( value < h->axis().lowerEdge() )value = h->axis().lowerEdge()+step;
    if( value > h->axis().upperEdge() )value = h->axis().upperEdge()-step;
  }

  if( m_splitSides && !(cellID == LHCb::CaloCellID()) ) {
    int col = cellID.col();
    int cal = cellID.calo();
    std::string side = "A-side";      
    if( CaloCellCode::CaloNameFromNum(cal)  == "Hcal"){
      if(col<16)side = "C-side";
    }
    else if(col<32){
      side = "C-side";
    }
    GaudiAlg::HistoID id(side + "/" + hid);
    const auto hh = m_h1[id];
    if( hh == nullptr ) return;
    fill(hh,value,w);
  }
  else if( m_split && !(cellID == LHCb::CaloCellID()) ) {
    std::string area = CaloCellCode::caloArea ( CaloCellCode::caloNum( m_detData ), cellID.area() );
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
      debug() << "Filling histogram2D per calo region " << cellID << endmsg;
    if( validArea( area ) ){
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
        debug() << "valid area " << area << endmsg;
      GaudiAlg::HistoID id(area + "/" + hid);
      const auto hh = m_h1[id];
      if( hh == nullptr ) return;
      fill(hh,value,w);
    }
  }
  fill(h,value,w);
}

void CaloMoniAlg::hFill2( LHCb::CaloCellID cellID , std::string hid, double x, double y, double w ) const { 
  if(!doHisto(hid)) return;
  const auto h = m_h2[hid];
  if( h == nullptr ) return;
  double xbins = (double) h->xAxis().bins();
  double xstep = h->xAxis().upperEdge() - h->xAxis().lowerEdge();
  xstep = ( xbins == 0) ? 0 : xstep/xbins/2.;
  double ybins = (double) h->yAxis().bins();
  double ystep = h->yAxis().upperEdge() - h->yAxis().lowerEdge();
  ystep = ( ybins == 0) ? 0 : ystep/ybins/2.;
  if(m_sat2D){
    if( x < h->xAxis().lowerEdge() )x = h->xAxis().lowerEdge()+xstep;
    if( x > h->xAxis().upperEdge() )x = h->xAxis().upperEdge()-xstep;
    if( y < h->yAxis().lowerEdge() )y = h->yAxis().lowerEdge()+ystep;
    if( y > h->yAxis().upperEdge() )y = h->yAxis().upperEdge()-ystep;
  }

  if( m_splitSides && !(cellID == LHCb::CaloCellID()) ) {
    int col = cellID.col();
    int cal = cellID.calo();
    std::string side = "A-side";      
    if( CaloCellCode::CaloNameFromNum(cal)  == "Hcal"){
      if(col<16)side = "C-side";
    }
    else if(col<32){
      side = "C-side";
    }
    GaudiAlg::HistoID id(side + "/" + hid);
    const auto hh = m_h2[id];
    fill(hh,x,y,w);
  }
  else if( m_split && !(cellID == LHCb::CaloCellID()) ){
    // std::string area = CaloCellCode::CaloAreaFromNum( CaloCellCode::CaloNumFromName( m_detData ), cellID.area() );
    std::string area = CaloCellCode::caloArea ( CaloCellCode::caloNum ( m_detData ), cellID.area() );
    if( validArea( area )  ){
      GaudiAlg::HistoID id(area + "/" + hid);
      const auto hh = m_h2[id];
      fill(hh,x,y,w); 
    }      
    fill(h,x,y,w); 
  }else{
    fill(h,x,y,w); 
  } 
}

//==============================================================================
// MISC
//==============================================================================

bool CaloMoniAlg::doHisto( const std::string histo ) const {
  // blacklist has highest priority
  if(std::any_of( m_removeHisto.begin(), m_removeHisto.end(), 
      [histo](std::string h){return histo == h;})) return false;
  // Followup by whitelist, "ALL"
  return std::any_of( m_histoList.begin(), m_histoList.end(),
      [histo](std::string h){return histo==h || "All"==h;});
}

//==============================================================================

bool CaloMoniAlg::validArea( const std::string area ) const {
  return std::any_of( m_areas.begin(), m_areas.end(), [area](auto s){return s==area;} );
}

//==============================================================================
