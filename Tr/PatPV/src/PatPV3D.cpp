// Include files
// -------------

// From Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Local
#include "PatPV3D.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PatPV3D
//
// 2004-02-17 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( PatPV3D )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PatPV3D::PatPV3D(const std::string& name,
                 ISvcLocator* pSvcLocator) :
MultiTransformerFilter(name , pSvcLocator,
                       KeyValue{"InputTracks", LHCb::TrackLocation::Default},
                       KeyValue("OutputVerticesName", LHCb::RecVertexLocation::Velo3D)) {
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<bool, std::vector<LHCb::RecVertex>>
PatPV3D::operator()(const std::vector<LHCb::Track>& inputTracks) const {

  MsgStream msg(msgSvc(), name());
  bool isDebug = msg.level() <= MSG::DEBUG;

  if (isDebug) {
    debug() << "==> Execute" << endmsg;
  }

  std::vector<LHCb::RecVertex> rvts;
  bool filter = false;
  StatusCode scfit = m_pvsfit->reconstructMultiPV(inputTracks, rvts);
  if (scfit == StatusCode::SUCCESS) {
    filter = !rvts.empty();
  } else {
    Warning("reconstructMultiPV failed!",scfit).ignore();
  }

  return std::make_tuple(filter, std::move(rvts));
}
