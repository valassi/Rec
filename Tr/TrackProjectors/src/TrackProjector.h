#ifndef TRACKPROJECTORS_TRACKPROJECTOR_H
#define TRACKPROJECTORS_TRACKPROJECTOR_H 1

// Include files

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/SystemOfUnits.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackProjector.h"

// TrackVectorFit
#include "TrackVectorFit/TrackVectorFit.h"

class IMagneticFieldSvc;
struct ITrajPoca;

/** @class TrackProjector TrackProjector.h TrackProjectors/TrackProjector.h
 *
 *  TrackProjector is the base class implementing methods
 *  from the ITrackProjector interface.
 *
 *  @author Jose Hernando
 *  @author Eduardo Rodrigues
 *  @author Sebastien Ponce
 */

class TrackProjector : public extends<GaudiTool, ITrackProjector> {

public:
   using base_class::base_class;

  /// ::initialize
  StatusCode initialize() override;

  /// Project a state onto a measurement.
  std::tuple<StatusCode, double, double>
  project( const LHCb::State& state, const LHCb::Measurement& meas ) const override;

  /// Project the state vector in this fitnode and update projection matrix and reference residual
  virtual StatusCode projectReference( LHCb::FitNode& node ) const override;
  virtual StatusCode projectReference( LHCb::Node& node ) const override;
  virtual StatusCode projectReference (::Tr::TrackVectorFit::Node& n) const override;

  /// Retrieve the derivative of the residual wrt. the alignment parameters
  /// of the measurement. The details of the alignment transformation are
  /// defined in AlignTraj.
  Derivatives alignmentDerivatives(const LHCb::StateVector& state,
                                   const LHCb::Measurement& meas,
                                   const Gaudi::XYZPoint& pivot) const override final;


  StatusCode project (
    const LHCb::StateVector& statevector,
    LHCb::Measurement& meas,
    Gaudi::TrackProjectionMatrix& H,
    double& residual,
    double& errMeasure,
    Gaudi::XYZVector& unitPocaVector,
    double& doca,
    double& sMeas
  ) const;

  virtual bool useBField() const override {
    return m_useBField;
  }

protected:
  /**
   * Helper struct storing the result of calls to internal_project
   */
  struct InternalProjectResult final {
    double sMeas;
    double doca;
    double residual;
    double errMeasure;
    Gaudi::TrackProjectionMatrix H;
    Gaudi::XYZVector unitPocaVector;
  };

  virtual InternalProjectResult
  internal_project(const LHCb::StateVector& statevector,
                   const LHCb::Measurement& meas) const;

  Gaudi::Property<bool> m_useBField { this,  "UseBField", false };                  /// Create StateTraj with or without BField information.
  Gaudi::Property<double> m_tolerance{ this, "Tolerance", 0.0005*Gaudi::Units::mm };   ///< Required accuracy of the projection
  IMagneticFieldSvc* m_pIMF = nullptr; ///< Pointer to the magn. field service
  ITrajPoca*         m_poca = nullptr; ///< Pointer to the ITrajPoca interface

};
#endif // TRACKPROJECTORS_TRACKPROJECTOR_H
