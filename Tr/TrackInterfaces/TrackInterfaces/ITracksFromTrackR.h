#ifndef TRACKINTERFACES_ITRACKSFROMTRACKR_H
#define TRACKINTERFACES_ITRACKSFROMTRACKR_H 1

// Include files
// from STL
#include <vector>
#include <range/v3/utility/any.hpp>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "PrKernel/UTHitHandler.h"

#include <boost/optional.hpp> // TODO replace with std::optional when c++17 available

// Forward declarations
namespace LHCb {
 class Track;
}

/** @class ITracksFromTrackR ITracksFromTrackR.h TrackInterfaces/ITracksFromTrackR.h
 *  Interface to the forward pattern tool, reentrant version
 */
class ITracksFromTrackR : public extend_interfaces<IAlgTool> {
public:

  DeclareInterfaceID( ITracksFromTrackR, 2, 0 );

  /// Create an instance of a state
  virtual ranges::v3::any createState() const = 0 ;

  /// Take an existing track and make new tracks from it (usually with hits from more detectors)
  virtual StatusCode tracksFromTrack( const LHCb::Track& seed,
                                      boost::optional<LHCb::Track>& tracks,
                                      ranges::v3::any& state) const  = 0;

};
#endif // TRACKINTERFACES_ITRACKSFROMTRACKR_H
