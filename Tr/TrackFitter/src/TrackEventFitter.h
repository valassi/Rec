#ifndef TRACKFITTER_TRACKEVENTFITTER_H
#define TRACKFITTER_TRACKEVENTFITTER_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"

// from TrackEvent
#include "Event/Track.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackFitter.h"

// Range v3
#include <range/v3/view.hpp>
#include <range/v3/range_for.hpp>

// TrackVectorFit
#include "Kernel/VectorSOAStore.h"

/** @class TrackEventFitter TrackEventFitter.h
 *
 *
 *  @author Eduardo Rodrigues
 *  @date   2005-05-30
 */
class TrackEventFitter : public Gaudi::Functional::Transformer<LHCb::Tracks(const std::vector<LHCb::Track>&)>{
public:
  /// Standard constructor
  TrackEventFitter( const std::string& name, ISvcLocator* pSvcLocator );
  /// initialization
  StatusCode initialize() override;
  /// finalization
  StatusCode finalize() override;
  /// main execution
  LHCb::Tracks operator()(const std::vector<LHCb::Track>&) const override;

private:
  /// interface to tracks fitter tool
  ToolHandle<ITrackFitter> m_tracksFitter;

  Gaudi::Property<bool> m_skipFailedFitAtInput{this, "SkipFailedFitAtInput", true};
  Gaudi::Property<double> m_maxChi2DoF{this, "MaxChi2DoF", 999999, "Max chi2 per track when output is a new container"};
  Gaudi::Property<unsigned> m_batchSize{this, "BatchSize", 50, "Size of batches"};

};
#endif // TRACKFITTER_TRACKEVENTFITTER_H
