// $Id: PatVeloDebugTool.h,v 1.3 2008-12-04 09:05:07 cattanem Exp $
#ifndef PATVELODEBUGTOOL_H
#define PATVELODEBUGTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "PatKernel/IPatDebugTool.h"            // Interface


/** @class PatVeloDebugTool PatVeloDebugTool.h
 *  Debug Velo processing using MC truth
 *
 *  @author Olivier Callot
 *  @date   2007-10-25
 */
class PatVeloDebugTool : public GaudiTool, virtual public IPatDebugTool {
public:
  /// Standard constructor
  PatVeloDebugTool( const std::string& type,
                    const std::string& name,
                    const IInterface* parent);

  virtual ~PatVeloDebugTool( ); ///< Destructor

  bool matchKey( LHCb::LHCbID& id, int key ) override;

  void printKey( MsgStream& msg, LHCb::LHCbID& id ) override;

  double xTrue( int key, double z ) override;

  double yTrue( int key, double z ) override;

protected:

private:

};
#endif // PATVELODEBUGTOOL_H
