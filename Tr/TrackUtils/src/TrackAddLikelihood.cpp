// track interfaces

#include "TrackInterfaces/ITrackFunctor.h"
#include "TrackAddLikelihood.h"

using namespace LHCb;

DECLARE_COMPONENT( TrackAddLikelihood )

TrackAddLikelihood::TrackAddLikelihood(const std::string& name, ISvcLocator* pSvcLocator): GaudiAlgorithm(name, pSvcLocator)
{
}

StatusCode TrackAddLikelihood::initialize() {
  std::transform( m_types.begin(), m_types.end(),
                  std::inserter(m_toolMap, m_toolMap.end() ),
                  [&](unsigned int t) {
    Track::History type = Track::History(t);
    auto name = Gaudi::Utils::toString(type)+"_likTool";
    return std::make_pair(type, tool<ITrackFunctor>(m_likelihoodToolName.value(),
                                              name, this ) );
  });
  return StatusCode::SUCCESS;
}

StatusCode TrackAddLikelihood::execute(){
  for (const LHCb::Track& track : *m_input.get() ) {
    unsigned int type = track.history();
    // FIXME : this is needed now that the input is a vector of Tracks
    // and no more Track*. However, it needs to be fixed as modification
    // of TES objects is no allowed and is thread unsafe
    LHCb::Track &modifiableTrack = const_cast<LHCb::Track&>(track);
    auto iter = m_toolMap.find(type);
    if (iter == m_toolMap.end()) {
      Warning("Likelihood not calculated: Unknown track type", StatusCode::SUCCESS ).ignore();
      modifiableTrack.setLikelihood( -999999. );
    } else {
      modifiableTrack.setLikelihood( (*iter->second)(track) );
    }
  }
  return StatusCode::SUCCESS;
}
