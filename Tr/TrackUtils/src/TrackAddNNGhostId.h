#ifndef TRACKADDNNGHOSTID_H
#define TRACKADDNNGHOSTID_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"


/** @class TrackAddNNGhostId TrackAddNNGhostId.h
 *
 *
 *  @author Johannes Albrecht
 *  @date   2009-10-06
 */
struct IGhostProbability;

class TrackAddNNGhostId : public GaudiAlgorithm {
public:
  /// Standard constructor
  TrackAddNNGhostId( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:
  IGhostProbability* m_ghostTool;

  std::string m_inputLocation;
  std::string m_ghostToolName;

};
#endif // TRACKADDNNGHOSTID_H
