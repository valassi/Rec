#ifndef _TrackAddLikelihood_H_
#define _TrackAddLikelihood_H_

/** @class TrackAddLikelihood TrackAddLikelihood.h
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Track.h"
#include <string>
#include <vector>
#include <map>

class TrackAddLikelihood final : public GaudiAlgorithm {
public:

  TrackAddLikelihood(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  StatusCode execute() override;

private:

  DataObjectReadHandle<std::vector<LHCb::Track>> m_input{ this, "inputLocation", LHCb::TrackLocation::Default, };
  Gaudi::Property<std::vector<unsigned int>> m_types { this, "types",
                                    { LHCb::Track::History::PatVelo,       LHCb::Track::History::PatVeloTT,
                                      LHCb::Track::History::PatForward,    LHCb::Track::History::TrackMatching,
                                      LHCb::Track::History::PatMatch,      LHCb::Track::History::PatSeeding,
                                      LHCb::Track::History::PatDownstream, LHCb::Track::History::TsaTrack,
                                      LHCb::Track::History::PatVeloGeneral,LHCb::Track::History::PatFastVelo } };
  Gaudi::Property<std::string>  m_likelihoodToolName { this, "LikelihoodTool", "TrackLikelihood" };
  std::map<unsigned int, const ITrackFunctor*> m_toolMap;

};

#endif
