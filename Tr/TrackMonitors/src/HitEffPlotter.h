#ifndef INCLUDE_HITEFFPLOTTER_H
#define INCLUDE_HITEFFPLOTTER_H 1

#include <vector>
#include <bitset>
#include <string>
#include <array>

#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/Vector3DTypes.h"

#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"
#include "TrackExpectedHitsXYZTool.h"

#include "Event/Track.h"

/** @class HitEffPlotter HitEffPlotter.h
 *
 * plot effective hit efficiencies for given track container as function of (x, y)
 *
 * @author Manuel Tobias Schiller <schiller@physi.uni-heidelberg.de>
 * @date   2010-06-10
 */

class HitEffPlotter : public Gaudi::Functional::Consumer<void(const LHCb::Tracks&),
                                                         Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>>

{
  public:

	/// Standard Constructor
	HitEffPlotter(const std::string& name, ISvcLocator* pSvcLocator);

	void operator()(const LHCb::Tracks&) const  override; ///< Algorithm event execution

  private:
	/// name of input track collection
    Gaudi::Property<bool> m_useUT { this, "UseUT", false };

	PublicToolHandle<IVeloExpectation> m_veloExpectation { this, "Velo", "VeloExpectation"};
	PublicToolHandle<IHitExpectation> m_utExpectation { this, "UT","UTHitExpectation"};
    PublicToolHandle<IHitExpectation> m_ttExpectation { this, "TT","TTHitExpectation"};
	PublicToolHandle<IHitExpectation> m_itExpectation { this, "IT","ITHitExpectation"};
	PublicToolHandle<IHitExpectation> m_otExpectation { this, "OT", "OTHitExpectation"};
	PublicToolHandle<TrackExpectedHitsXYZTool> m_xyzExpectation { this, "XYZTool", "TrackExpectedHitsXYZTool"};

	/// plot hit efficiency numerator and denominator as function of (x, y)
	template<size_t N> void plot(
		std::string namepfx, std::string titlepfx,
		unsigned nxbins, const double xmin, const double xmax,
		unsigned nybins, const double ymin, const double ymax,
		const std::bitset<N>& expected, const std::bitset<N>& ontrack,
		const std::array<Gaudi::XYZVector, N>& points) const;


};
#endif // INCLUDE_HITEFFPLOTTER_H

