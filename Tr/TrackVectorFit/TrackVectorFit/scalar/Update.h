#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

namespace TrackVectorFit {

namespace Scalar {

template<class D>
inline void update (
  Node& node
) {
  if (node.node().type() == LHCb::Node::HitOnTrack) {
    Math::update (
      node.get<D, Op::StateVector>(),
      node.get<D, Op::Covariance>(),
      node.get<D, Op::Chi2>(),
      node.get<Op::NodeParameters, Op::ReferenceVector>(),
      node.get<Op::NodeParameters, Op::ProjectionMatrix>(),
      node.get<Op::NodeParameters, Op::ReferenceResidual>(),
      node.get<Op::NodeParameters, Op::ErrMeasure>()
    );
  }
}

}

}

}
