#ifndef PTTRANSPORTER_H
#define PTTRANSPORTER_H 1

// Include files
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "TrackInterfaces/IPtTransporter.h"            // Interface


/** @class PtTransporter PtTransporter.h
 *
 * provide a fast way to calculate a pt estimate in the velo, given a
 * state after the magnet
 *
 * @author Manuel Tobias Schiller <schiller@phys.uni-heidelberg.de>
 * @date   2008-04-16
 */
class PtTransporter : public extends<GaudiTool, IPtTransporter > {
public:
  /// Standard constructor
  using base_class::base_class;

  double ptAtOrigin(double zref, double xref, double yref,
                    double tx, double ty, double p) const override;
  double ptAtOrigin(const LHCb::State& state) const override;

private:
  Gaudi::Property<double> m_zMagnet { this, "zMagnet", 5300. * Gaudi::Units::mm };
};
#endif // PTTRANSPORTER_H
