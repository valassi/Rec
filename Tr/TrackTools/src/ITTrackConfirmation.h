//-----------------------------------------------------------------------------
/** @file ITTrackConfirmation.h
 *
 *  Header file for reconstruction tool : ITTrackConfirmation
 *
 *  @author M.Needham Matt.Needham@cern.ch
 *  @date   22/10/2008
 */
//-----------------------------------------------------------------------------

#ifndef TRACKTOOLS_ITTrackConfirmation_H
#define TRACKTOOLS_ITTrackConfirmation_H


//-----------------------------------------------------------------------------

#include "GaudiAlg/GaudiHistoTool.h"
#include "TrackInterfaces/ITrackSelector.h"
#include <string>
#include "Event/Track.h"


struct ITrackCaloMatch;

class ITTrackConfirmation : public extends<GaudiHistoTool, ITrackSelector> {

public:

  /// constructor
  using base_class::base_class;

  /// Tool initialization
  StatusCode initialize() override;

  /** Returns if the given track is selected or not
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return boolean indicating if the track is selected or not
   *  @retval true  Track is selected
   *  @retval false Track is rejected
   */
  bool accept ( const LHCb::Track& aTrack ) const override;

private:

  bool selfConfirm(const LHCb::Track& aTrack) const;

  Gaudi::Property<double> m_maxChi2Cut { this, "MaxChi2Cut", 1000 };
  Gaudi::Property<double> m_minCaloEnergy { this, "MinCaloEnergy", 0.0*Gaudi::Units::MeV };
  Gaudi::Property<bool> m_selfConfirm { this, "selfConfirm", true };
  Gaudi::Property<bool> m_caloConfirm { this, "caloConfirm", true };

  ITrackCaloMatch* m_trackCaloMatch = nullptr;

};

#endif // TRACKTOOLS_ITTrackConfirmation_H
