#ifndef TRACKTOOLS_TRACKCHI2CALCULATOR_H
#define TRACKTOOLS_TRACKCHI2CALCULATOR_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// local
#include "TrackInterfaces/ITrackChi2Calculator.h"

/** @class TrackChi2Calculator TrackChi2Calculator.h "TrackTools/TrackChi2Calculator.h"
 *
 *  This tool can be used to calculate the chi2-distance between
 *  two track states.
 *
 *  @author Jeroen van Tilburg
 *  @date   2003-09-18
 */

class TrackChi2Calculator : public extends<GaudiTool, ITrackChi2Calculator> {
public:
  /// Standard constructor
  using base_class::base_class;

  /** Calculate the chi2 distance between two track vectors.
   *  The track vectors must be given as (x,y,tx,ty,q/p).
   *  @return StatusCode: Failure if matrix inversion failed
   *  @param  trackVector1 input 1st track HepVector
   *  @param  trackCov1 input covariance matrix corresponding to 1st vector
   *  @param  trackVector2 input 2nd track HepVector
   *  @param  trackCov2 input covariance matrix corresponding to 2nd vector
   *  @param  chi2 output chi2 distance between the two vectors
   */
  StatusCode calculateChi2( const Gaudi::TrackVector& trackVector1,
                            const Gaudi::TrackSymMatrix& trackCov1,
                            const Gaudi::TrackVector& trackVector2,
                            const Gaudi::TrackSymMatrix& trackCov2,
                            double& chi2 ) const override;

private:
  StatusCode calculateChi2( Gaudi::Vector4& trackVector1,
                            Gaudi::Vector4& trackVector2,
                            Gaudi::SymMatrix4x4& trackCov12,
                            double& chi2 ) const;

  /// invert the 5x5 matrix
  StatusCode invertMatrix( Gaudi::TrackSymMatrix& invC ) const;

  /// invert the 4x4 matrix
  StatusCode invertMatrix( Gaudi::SymMatrix4x4& invC ) const;

  // job options
  // -----------
  /// Re-scale the track covariance matrices with this vector
  Gaudi::Property<std::vector<double>> m_scaleVector { this,  "ScaleVector" };
  /// Remove Tx from chi2 contribution in case of matching inside the magnet
  /// with straight lines
  Gaudi::Property<bool> m_matchInMagnet { this,  "MatchInMagnet", false };
  /// Add momentum information to the matching chi2 criterium
  Gaudi::Property<bool> m_addMomentum { this,  "AddMomentum", false };

};
#endif // TRACKTOOLS_TRACKCHI2CALCULATOR_H
