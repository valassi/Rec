#ifndef _TrackLikelihood_H
#define _TrackLikelihood_H

#include "TrackInterfaces/ITrackFunctor.h"
#include "GaudiAlg/GaudiTool.h"

/** @class TrackLikelihood TrackLikelihood.h
 *
 * Implementation of TrackLikelihood tool
 * see interface header for description
 *
 *  @author M.Needham
 *  @date   22/5/2007
 */

struct IHitExpectation;
struct IVeloExpectation;

namespace LHCb{
  class Track;
}

class TrackLikelihood: public extends<GaudiTool, ITrackFunctor>  {

 public:

  /** constructor */
  using base_class::base_class;

  StatusCode initialize() override;

  float operator()(const LHCb::Track& aTrack) const override;

 private:

  double addVelo(const LHCb::Track& aTrack) const;
  double addTT(const LHCb::Track& aTrack) const;
  double addUT(const LHCb::Track& aTrack) const;
  double addOT(const LHCb::Track& aTrack) const;
  double addIT(const LHCb::Track& aTrack) const;

  IVeloExpectation* m_veloExpectation = nullptr;
  IHitExpectation* m_ttExpectation = nullptr;
  IHitExpectation* m_utExpectation = nullptr;
  IHitExpectation* m_itExpectation = nullptr;
  IHitExpectation* m_otExpectation = nullptr;

  Gaudi::Property<double> m_veloREff{ this, "veloREff", 0.965 };
  Gaudi::Property<double> m_veloPhiEff{ this, "veloPhiEff", 0.94 };
  Gaudi::Property<double> m_otEff{ this, "otEff", 0.93 };
  Gaudi::Property<double> m_itEff{ this, "itEff", 0.995 };
  Gaudi::Property<double> m_ttEff{ this, "ttEff", 0.99 };
  Gaudi::Property<double> m_utEff{ this, "utEff", 0.99 };
  Gaudi::Property<double> m_veloHighEff1{ this, "veloHighEff1", 0.99 };
  Gaudi::Property<double> m_veloHighEff2{ this, "veloHighEff2", 0.995 };
  Gaudi::Property<double> m_itHighEff{ this, "itHighEff", 0.94 };
  Gaudi::Property<double> m_ttHighEff{ this, "ttHighEff", 0.896 };
  Gaudi::Property<double> m_utHighEff{ this, "utHighEff", 0.896 };

  Gaudi::Property<double> m_chiWeight{ this, "chiWeight", 1.0 };
  Gaudi::Property<bool> m_useVelo{ this, "useVelo", true };
  Gaudi::Property<bool> m_useTT{ this, "useTT", true };
  Gaudi::Property<bool> m_useUT{ this, "useUT", false };
  Gaudi::Property<bool> m_useIT{ this, "useIT", true };
  Gaudi::Property<bool> m_useOT{ this, "useOT", true };

};

#endif
