// Include files

// local
#include "PrGECFilter.h"
#include "STDAQ/UTDAQHelper.h"
#include "FTDAQ/FTDAQHelper.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrGECFilter )

/// Standard constructor, initializes variables
PrGECFilter::PrGECFilter(const std::string& name,
                         ISvcLocator* pSvcLocator) :
FilterPredicate(name, pSvcLocator,
                {KeyValue{"RawEventLocations",
                      Gaudi::Functional::concat_alternatives(LHCb::RawEventLocation::Tracker,
                                                             LHCb::RawEventLocation::Other,
                                                             LHCb::RawEventLocation::Default)}}) {}

bool PrGECFilter::operator()(const LHCb::RawEvent& rawEvt) const {
  // do not work for nothing !
  if (m_nFTUTClusters <= 0) {
    return true;
  }

  // check UT clusters
  auto nbUTClusters = LHCb::UTDAQ::nbUTClusters(rawEvt.banks(LHCb::RawBank::UT), m_nFTUTClusters);
  if (!nbUTClusters) return false;

  // check FT clusters
  return static_cast<bool>(LHCb::FTDAQ::nbFTClusters(rawEvt.banks(LHCb::RawBank::FTCluster),
                                                     m_nFTUTClusters-nbUTClusters.get(),
                                                     m_clusterMaxWidth));
}
