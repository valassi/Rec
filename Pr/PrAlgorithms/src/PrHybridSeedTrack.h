#ifndef PRHYBRIDSEEDTRACK_H
#define PRHYBRIDSEEDTRACK_H 1

// Include files
#include "Event/Track.h"
#include "PrKernel/PrHit.h"

/** @class PrHybridSeedTrack PrHybridSeedTrack.h
 *  This is the working class inside the T station pattern
 *
 *  @author Renato Quagliani
 *  @date   2015-05-13
 */
//Comment it if you don't want to do truth matching
#include "LHCbMath/BloomFilter.h"

class PrHybridSeedTrack final{
public:
  typedef BloomFilter<LHCb::LHCbID, 6, 98, 16384>  XHitFingerPrint;
  const XHitFingerPrint& bloomfilter() const noexcept{
    return m_ids;
  }
private:
  XHitFingerPrint m_ids;
public:
  void updateIDs() noexcept{
    m_ids.clear();
    for(const auto& hit : m_hits){
      m_ids.insert(hit.hit->id());
    }
  }
public:
  /// Constructor with the z reference
  PrHybridSeedTrack( unsigned int zone, float zRef )
    : m_zone( zone ), m_zRef( zRef ){
    init();
  };

  PrHybridSeedTrack( unsigned int zone, float zRef, ModPrHits& hits )
    : m_zone{zone}, m_zRef{zRef}, m_hits{hits} {
    init();
  };
  //Handling the hits on the track ( PrHits is vector<PrHit*> )
  ModPrHits& hits() noexcept {return m_hits;}
  const ModPrHits& hits()  const noexcept{ return m_hits;  }
  void addHit(const ModPrHit& hit) noexcept { m_hits.push_back(hit); }
  void addHits(const ModPrHits& hits) noexcept {
    m_hits.reserve( m_hits.size() + hits.size() );
    m_hits.insert( m_hits.end(), hits.begin(), hits.end() );
  }
  //Zone (0,1) Up Down
  unsigned int zone() const noexcept{ return m_zone; }
  //====================================
  //SETTERS & Getters  Genearl One
  //====================================
  //***********Track Parameters to compute distances
  void setParameters( float ax, float bx, float cx, float ay, float by ) noexcept{
    m_ax = ax;
    m_bx = bx;
    m_cx = cx;
    m_ay = ay;
    m_by = by;
  }
  float ax()                      const noexcept { return m_ax;}
  float bx()                      const noexcept { return m_bx;}
  float cx()                      const noexcept { return m_cx;}
  float ay()                      const noexcept { return m_ay;}
  float by()                      const noexcept { return m_by;}

  //**********Update the parameters of a track iteratively in the Fit
  void updateParameters( float dax, float dbx, float dcx,
                         float day=0., float dby= 0.  ) {
    m_ax += dax;
    m_bx += dbx;
    m_cx += dcx;
    m_ay += day;
    m_by += dby;
  }
  void setYParam( float ay=0., float by=0.){
    m_ay = ay;
    m_by = by;
  }

  void setXT1( float value){
    m_xT1 = value;
  }
  float xT1( ) const noexcept {
    return m_xT1;
  }
  void setXT3( float value){
    m_xT3 = value;
  }
  float xT3( )const noexcept {
    return m_xT3;
  }
  void setXT2(float value){
    m_xT2 = value;
  }
  float xT2()const noexcept {
    return m_xT2;
  }

  void setnXnY( unsigned int nx, unsigned int ny){
    m_nx = nx;
    m_ny = ny;
  }
  unsigned int nx() const noexcept {return m_nx;}
  unsigned int ny() const noexcept {return m_ny;}


  //validity
  void setValid( bool v )               { m_valid = v; }
  bool   valid()                 const noexcept { return m_valid; }

  void setRecovered( bool v){ m_recovered = v; }
  bool recovered() const noexcept { return m_recovered; }
  //dRatio
  void setdRatio( float dRatio )       { m_dRatio = dRatio;}
  float dRatio()                 const noexcept {return m_dRatio;}

  // chi2
  void setChi2(float chi2, int nDoF) { m_chi2 = chi2;m_nDoF = nDoF;m_chi2DoF = m_chi2/m_nDoF; }
  float chi2()                   const noexcept { return m_chi2; }
  float chi2PerDoF()         const noexcept { return m_chi2DoF;}//m_chi2 / m_nDoF;}
  int nDoF()                        const noexcept { return m_nDoF; }

  //BackProjection
  void setX0(float X0)                  { m_X0=X0;}
  float X0()                      const noexcept { return m_X0;}
  float y( float z )          const noexcept { return (m_ay + m_by*z);}
  //Slope by
  float ySlope( )               const noexcept { return m_by; }

  //X at a given Z
  float x( float z )const noexcept {
    const float dz = z-m_zRef;
    return m_ax + dz * ( m_bx + dz* m_cx * (1.+m_dRatio*dz) );
  }
  //Slope X-Z plane track at a given z
  float xSlope( float z )const noexcept {
    const float dz = z-m_zRef;
    return m_bx + 2.*dz*m_cx + 3.*dz*dz*m_cx*m_dRatio; //is it need?
  }
  void setCase(unsigned int Case){
    m_case=Case ;
  }
  unsigned int Case() const noexcept {
    return m_case;
  }
  //y positon on Track
  inline float yOnTrack( const PrHit& hit ) const noexcept { return hit.yOnTrack( m_ay , m_by ); }
  //distance from Hit of the track
  inline float distance( const PrHit& hit ) const noexcept {
    float yTra = yOnTrack( hit ); //is it needed for x - layers?
    return hit.x( yTra ) - x( hit.z(yTra) );
  }

  float deltaY( const PrHit& hit )        const noexcept {
    if ( hit.isX() ) return 0.f;
    return distance( hit ) / hit.dxDy();
  }

  inline float chi2( const PrHit& hit )           const noexcept {
    const float d = distance( hit );
    float w = hit.w();
    return d * d * w;
  }

  struct GreaterBySize{//Before elements with lower n hits later and lower Chi2
    inline bool operator() (const PrHybridSeedTrack& lhs, const PrHybridSeedTrack& rhs) const noexcept {
      if( lhs.hits().size() == rhs.hits().size() ){
        return lhs.chi2PerDoF() > rhs.chi2PerDoF();
      }
      return lhs.hits().size() < rhs.hits().size();
      //< by default
    }
  };

  struct LowerBySize{ //Before higher number of hits and better chi2
    inline bool operator() ( const PrHybridSeedTrack& lhs, const PrHybridSeedTrack& rhs) const noexcept {
      if(lhs.hits().size() == rhs.hits().size()){
        return lhs.chi2PerDoF() < rhs.chi2PerDoF();
      }
      return lhs.hits().size() > rhs.hits().size();
    }
  };

  unsigned int size() const noexcept {
    return m_hits.size();
  };

protected:

private:

  void init(){
    m_valid = true;
    m_recovered = false;
    m_hits.reserve( 32 );
    m_case = -1;
    m_nx = 0;
    m_ny = 0;
    //track params
    m_ax = 0.f;
    m_bx = 0.f;
    m_cx = 0.f;
    m_by = 0.f;
    m_ay = 0.f;
    //track chi2 settings
    m_chi2 = 0.f;
    m_chi2DoF = 0.f;
    m_nDoF = -1;
    m_dRatio = 0.f;
    m_X0 = std::numeric_limits<float>::min();
  }
  //Global Private variables
private:
  float m_chi2DoF;
  unsigned int m_nx;
  unsigned int m_ny;
  // int m_nUVLine;
  unsigned int m_zone;
  float        m_zRef;
  ModPrHits    m_hits;
  bool         m_valid;
  bool m_recovered;
  int         m_nDoF;
  float       m_chi2;
  float       m_ax;
  float       m_bx;
  float       m_cx;
  float       m_ay;
  float       m_by;
  float       m_dRatio;
  float       m_X0;
  float       m_xT1;
  float       m_xT2;
  float       m_xT3;
  unsigned int m_case;
};

typedef std::vector<PrHybridSeedTrack> PrHybridSeedTracks;

#endif // PRHYBRIDSEEDTRACK
