################################################################################
# Package: PrAlgorithms
################################################################################
gaudi_subdir(PrAlgorithms v1r18)

gaudi_depends_on_subdirs(Det/FTDet
                         Det/STDet
                         Event/DAQEvent
                         Event/RecEvent
                         Tr/TrackInterfaces
                         Pr/PrKernel
                         Rec/LoKiTrack)

find_package(Boost)
find_package(ROOT)
find_package(Vc)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${Vc_INCLUDE_DIR})

gaudi_add_module(PrAlgorithms
                 src/*.cpp
                 INCLUDE_DIRS Tr/TrackInterfaces FT/FTDAQ Vc
                 LINK_LIBRARIES FTDetLib DAQEventLib RecEvent PrKernel STDetLib Vc FTDAQLib STDAQLib LoKiTrackLib)

