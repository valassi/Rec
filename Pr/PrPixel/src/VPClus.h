#ifndef VPCLUS_H
#define VPCLUS_H 1

#include <array>
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VPConstants.h"
#include "PrPixelModule.h"
#include "VPDet/DeVP.h"
#include "VeloPixelInfo.h"

//FIXME still need to handle nx and ny for the clusters

// Namespace for locations in TDS
namespace LHCb
{
  namespace VPClusterLocation
  {
    static const std::string Offsets = "Raw/VP/LightClustersOffsets";
  }
}

struct SensorCache {
  // representing the transformed pitch vectors
  float xx, xy, yx, yy;
  // coordinates (xi, yi) of pixel in lower left corner of chip
  std::array<float, VeloInfo::Numbers::NChipCorners> ChipCorners;
  // sensor z position
  float z;
};

struct SPCache {
  std::array<float, 4> fxy;
  unsigned char pattern;
  unsigned char nx1;
  unsigned char nx2;
  unsigned char ny1;
  unsigned char ny2;
};

class VPClus
  : public Gaudi::Functional::MultiTransformer<std::tuple<std::vector<LHCb::VPLightCluster>,
                                                          std::array<unsigned, VeloInfo::Numbers::NOffsets>>(
          const LHCb::RawEvent& )>
{

public:
  /// Standard constructor
  VPClus( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm initialization
  StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned,VeloInfo::Numbers::NOffsets >>
  operator()( const LHCb::RawEvent& ) const override;

private:
  /// Cache Super Pixel patterns for isolated Super Pixel clustering.
  void cacheSPPatterns();

  /// Recompute the geometry in case of change
  StatusCode rebuildGeometry();
  //
  /// Detector element
  DeVP* m_vp{nullptr};

  /// List of pointers to modules (which contain pointers to their hits)
  /// FIXME This is not thread safe on a change on geometry.
  /// FIXME These vectors should be stored as a derived condition
  std::vector<PrPixelModule*> m_modules;
  std::vector<PrPixelModule> m_module_pool;

  /// Indices of first and last module
  unsigned int m_firstModule;
  unsigned int m_lastModule;

  /// Maximum allowed cluster size (no effect when running on lite clusters).
  unsigned int m_maxClusterSize = 999999999;

  float m_ltg[16 * VP::NSensors]; // 16*208 = 16*number of sensors
  const double *m_local_x;
  const double *m_x_pitch;
  float m_pixel_size;

  // SP pattern buffers for clustering, cached once.
  // There are 256 patterns and there can be at most two
  // distinct clusters in an SP.
  std::array<SPCache, 256> m_SPCaches{};

  Gaudi::Property<std::vector<unsigned int>> m_modulesToSkip{ this, "ModulesToSkip",{}, "List of modules that should be skipped in decoding"};
  std::bitset<VP::NModules> m_modulesToSkipMask;
};

#endif // VPClus_H

