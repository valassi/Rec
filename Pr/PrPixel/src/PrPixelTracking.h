#ifndef PRPIXELTRACKING_H
#define PRPIXELTRACKING_H 1

 //#define DEBUG_HISTO // fill some histograms while the algorithm runs.

// Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "VPDet/DeVP.h"

// Local
#include "VPClus.h"
#include "PrPixelTrack.h"
#include "VeloPixelInfo.h"

#include <boost/container/static_vector.hpp>
#include "Kernel/STLExtensions.h"
enum Conf{
  Default =0, 
  Forward =1, 
  Backward =2};
		   
//Overall configuration of tracking strategy in PixelTracking, which can be passed as argument as specified in the PrPixelTracking.cpp first lines
namespace VPConf{
	enum class ConfAlgo{ 
		 DefaultAlgo =0, 
		 OnlyForward =1, 
		 OnlyBackward =2, 
		 ForwardThenBackward =3, 
		 BackwardThenForward=4 
	};
	std::string toString(const ConfAlgo& configuration);
 	 std::ostream& toStream(const ConfAlgo& configuration, std::ostream& os)
 	 { return os << std::quoted(toString(configuration),'\''); }
 	 StatusCode parse(ConfAlgo& result, const std::string& input );
 	 // and allow printout..
 	 inline std::ostream& operator<<(std::ostream& os, const ConfAlgo& s) { return toStream(s,os); }
}

/** @class PrPixelTracking PrPixelTracking.h
 *  This is the main tracking for the Velo Pixel upgrade
 *
 *  @author Olivier Callot
 *  @author Sebastien Ponce
 */


/**
  Pattern recognition is achieved using the hitbuffer container.
  It simply contains a list of size_t indices pointing to the index where the hit is placed in TES
  inside the flat container.
  Thus, hibuffer[foundhit-1], is the last hit found added on the track
  Clusters in TES are sorted by Phi value and offsets indices indicates in order the starting point in the vector of
  moduleID.
  Sorting is done as module = [0,1,2,3,4,5,6,7.....,51]
  You have 53 offsets value pointing to [module0beg, module1beg, .....module51beg, module51end]
  The beg of next module is the end of previous.
  We sort the clusters by phi in each module in VPClus algorithm, thus we have to recompute at the beginning of the algorithm
  the value of phi of each hit, stored in the hits_phi vector.
*/

 class PrPixelTracking : public Gaudi::Functional::Transformer<std::vector<LHCb::Track>( const std::vector<LHCb::VPLightCluster>&,
                                                                                         const std::array<unsigned, VeloInfo::Numbers::NOffsets>& )
#ifdef DEBUG_HISTO
                                                      ,
                                                  Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>
#endif
>{

 public:
  /// Standard constructor
  PrPixelTracking(const std::string &name, ISvcLocator *pSvcLocator);

  /// Algorithm initialization
  StatusCode initialize() override;

  /// Algorithm execution
   std::vector<LHCb::Track> operator()(const std::vector<LHCb::VPLightCluster>& clusters,
                                      const std::array<unsigned, VeloInfo::Numbers::NOffsets>&) const override;

 private:

   /// Compute and store aligned to VPLightCluster ordering the phi values (requires sorting to be done in clustering)
  void getPhi( const std::vector<LHCb::VPLightCluster> & clusters,
          const std::array<unsigned, VeloInfo::Numbers::NOffsets> & offsets,
          std::vector<float>& phivec) const;

  /// Recompute the geometry in case of change
  StatusCode recomputeModuleZPositions();

  /// Extrapolate a seed track and try to add further hits.
  template<Conf configuration>
    void extendTrack( const std::vector<LHCb::VPLightCluster>& clusters,
                      const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                      boost::container::static_vector<size_t,35>& hitbuffer,
                      int currentModule,
                      const std::vector<float>& phi_hits,
                      std::vector<unsigned char>& clusterIsUsed
                      ) const;
  
  template<Conf configuration>
    void doPairSearch( const std::vector<LHCb::VPLightCluster>& clusters,
                       std::vector<unsigned char>& clusterIsUsed,
                       const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                       std::vector<LHCb::Track>& outputTracks,
                       std::vector<size_t>& ThreeHitVec,
                       PrPixelTrack& FitTrack,
                       boost::container::static_vector<size_t,35>& hitbuffer,
                       const std::vector<float>& phi_hits) const;

  template< Conf configuration>
    inline void updatenextstationsearch( int & next, int value)const;

  /// Search for tracks starting from pair of hits on adjacent sensors
  void searchByPair( const std::vector<LHCb::VPLightCluster>& clusters,
                     const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                     std::vector<LHCb::Track>& outputTracks,
                     const std::vector<float>& phi_hits ) const;
   
  /// Produce LHCb::Track list understandable to other LHCb applications.
  template< Conf configuration>
   void makeLHCbTracks( PrPixelTrack& track,
                        const boost::container::static_vector<size_t,35>& hitbuffer,
                        const std::vector<LHCb::VPLightCluster>& clusters,
                        std::vector<LHCb::Track>& outputTracks ) const;

  /// Try to add a matching hit on a given module.
  size_t bestHitSameSide( const std::vector<LHCb::VPLightCluster>& clusters,
                          const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                          size_t next,
                          unsigned foundHits,
                          boost::container::static_vector<size_t,35>& hitbuffer,
                          const std::vector<float>& phi_hits,
                          std::vector<unsigned char>& clusterIsUsed) const;

  size_t bestHitChangeSide( const std::vector<LHCb::VPLightCluster>& clusters,
                            const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                            size_t next,
                            unsigned foundHits,
                            boost::container::static_vector<size_t,35>& hitbuffer,
                            const std::vector<float>& phi_hits,
                            size_t moduleIDlastAdded,
                            std::vector<unsigned char>& clusterIsUsed) const;

   inline float maxPhiModule( unsigned moduleID,  const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,const std::vector<float>& phi_hits )const;
   inline float minPhiModule( unsigned moduleID,  const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,const std::vector<float>& phi_hits )const;

  void printTrack(PrPixelTrack& track) const;

  /// Properties
  Gaudi::Property<float> m_maxXSlope{this, "MaxXSlope", 0.400, "X Slope limit for seed pairs"};
  Gaudi::Property<float> m_maxYSlope{this, "MaxYSlope", 0.400, "Y Slope limit for seed pairs"};
  Gaudi::Property<float> m_extraTol{this, "ExtraTol", 0.6 * Gaudi::Units::mm, "Tolerance window when adding hits"};
  Gaudi::Property<unsigned int> m_maxMissedConsecutive{this, "MaxMissedConsecutive", 1,
      "Number of consective pairs of modules (left/right) without a hit after which to stop extrapolation!"};
  Gaudi::Property<unsigned int> m_maxMissedOnTrack{this, "MaxMissedOnTrack", 3,
      "Number of pairs of modules (left/right) without a hit on the full track building when extrapolating"};
  Gaudi::Property<float> m_maxScatter{this, "MaxScatter", 0.004, "Acceptance criteria for adding new hits"};
  Gaudi::Property<float> m_maxChi2Short{this, "MaxChi2Short", 20.0,
      "Acceptance criteria for track candidates : Max. chi2 for 3-hit tracks"};
   Gaudi::Property<float> m_fractionUnused{this, "FractionUnused", 0.5,
       "Acceptance criteria for track candidates : Min. fraction of unused hits"};
   Gaudi::Property<bool> m_stateClosestToBeamKalmanFit{this, "ClosestToBeamStateKalmanFit", true,
       "Parameter for Kalman fit"};
   Gaudi::Property<bool> m_stateEndVeloKalmanFit{this, "EndVeloStateKalmanFit", false, "Parameter for Kalman fit"};
   Gaudi::Property<bool> m_addStateFirstLastMeasurementKalmanFit{this, "AddFirstLastMeasurementStatesKalmanFit", false,
       "Parameter for Kalman fit"};
   Gaudi::Property<VPConf::ConfAlgo> m_ConfAlgo { this, "AlgoConfig", VPConf::ConfAlgo::DefaultAlgo };
   //Specific settings for Forward and Backward search
   //Values -100. , +100. is used due to MC studies looking to tracks with eta >0 and eta <0 observing that dr/dz is expected to be >0 ( <0 ) for tracks with hits at z>-100. (<100.) mm  
   Gaudi::Property<float> m_ForwardTracks_minZ{ this,  "MinZ_ForwardTracks", -100.,   "Break doublet building when searching Forward Tracks at z = MinZ_ForwardTracks [mm]"};
   Gaudi::Property<float> m_BackwardTracks_maxZ{this, "MaxZ_BackwardTracks", +100.,   "Break doublet building when searching Backward Tracks at z = MaxZ_ForwardTracks [mm]"};


   Gaudi::Property<bool> m_hardFlagging{ this, "HardFlagging", false,"Do not re-use flagged hits, flag also 3 hit tracks and break"};
   Gaudi::Property<bool> m_skiploopsens{ this, "SkipLoopSens", false, "Skip 1 station in pair creation"};
   Gaudi::Property<float> m_PhiPairs{ this,      "PhiWindow", 5.5, "Tolerance in DeltaPhi to pick hits in doublet search for the modules"};
   Gaudi::Property<float> m_PhiExtrap{ this, "PhiWindowExtrapolation", 5.5, "Tolerance in DeltaPhi to pick hits in hit propagation"};
   Gaudi::Property<std::vector<unsigned int>> m_modulesToSkip{ this, "ModulesToSkip", {}, "List of modules that should be skipped in decoding and tracking"};
   Gaudi::Property<std::vector<unsigned int>> m_modulesToSkipForPairs{ this, "ModulesToSkipForPairs", {}, "List of modules that should be skipped when creating seed pairs but not when propagating tracks"};

   Gaudi::Property<bool> m_earlykill3hittracks{this, "EarlyKill3HitTracks", false, "Enforce kill of triplets same side (from consecutive modules and early check in Phi)"};

  /// List of modules to be skipped in decoding and tracking
   std::bitset<VP::NModules> m_modulesToSkipMask;
   /// List of modules to be skipped when creating seed pairs
   std::bitset<VP::NModules> m_modulesToSkipForPairsMask;
   /// Detector element
   DeVP *m_vp;

   /// z positions of the modules
   std::array<float, VP::NModules> m_moduleZPositions{};

   /// first and last module
   unsigned int m_firstModule;
   unsigned int m_lastModule;

   float m_maxScatterSq = 0.0;
   const size_t badhit = 999999999;
};
#endif  // PRPIXELTRACKING_H
