#ifndef PRFITTOOL_H 
#define PRFITTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "GaudiKernel/Point3DTypes.h"

#include "PrFitParams/LinParFit.h"
#include "PrFitParams/IPrFitTool.h"

/** @class PrFitTool PrFitTool.h
 *  
 *
 *  @author Olivier Callot
 *  @date   2006-12-08
 */

class PrFitTool final : public extends<GaudiTool, IPrFitTool>
{
public: 
  /// Standard constructor
  PrFitTool( const std::string& type, 
           const std::string& name,
           const IInterface* parent);
  
  virtual boost::optional<std::tuple<double, double>>
      fitLine(const std::vector<Gaudi::XYZPoint>& hit, XY mode, double z0) const override;
  
  virtual boost::optional<std::tuple<double, double, double>>
      fitParabola(const std::vector<Gaudi::XYZPoint>& hit, XY mode, double z0) const override;
  
  virtual boost::optional<std::tuple<double, double, double, double>>
      fitCubic(const std::vector<Gaudi::XYZPoint>& hit, XY mode, double z0) const override;

private:

  mutable LinParFit<double> m_fit2;
  mutable LinParFit<double> m_fit3;
  mutable LinParFit<double> m_fit4;

};
#endif // PRFITTOOL_H
