#ifndef PRKERNEL_UTHITHANDLER_H
#define PRKERNEL_UTHITHANDLER_H 1

// Include files
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "GaudiKernel/Range.h"
#include "PrKernel/UTHitInfo.h"
#include "PrKernel/UTHit.h"
#include "TfKernel/IndexedHitContainer.h"
#include "TfKernel/MultiIndexedHitContainer.h"

/** @class UTHitHandler UTHitHandler.h PrKernel/UTHitHandler.h
 *
 *  UTHitHandler contains the hits in the UT detector and the accessor to them
 *  TODO: convert it to a 2D IndexedHitContainer.
 *  @author Renato Quagliani, Christoph Hasse
 *  @date   2016-11-16
 */
namespace UT
{
  class HitHandler
  {
  public:
    /// Standard constructor
    using HitContainer = MultiIndexedHitContainer<UT::Hit, UT::Info::kNStations, UT::Info::kNLayers>;

    using HitRange = typename HitContainer::HitRange;

    HitHandler()                              = default;
    HitHandler( const HitHandler& other ) = default;
    HitHandler( HitHandler&& other )      = default;
    HitHandler& operator=( const HitHandler& other ) = default;
    HitHandler& operator=( HitHandler&& other ) = default;

    // Constructor with the capacity of the HitContainer
    HitHandler(int size) : m_hits(size) {};

    // Method to add Hit in the container
    void AddHit( const DeSTSector* aSector, const LHCb::STLiteCluster& cluster )
    {
      auto station = cluster.channelID().station() - 1;
      auto layer   = cluster.channelID().layer() - 1;
      double dxDy;
      double dzDy;
      double xAtYEq0;
      double zAtYEq0;
      double yBegin;
      double yEnd;
      //--- this method allow to set the values
      aSector->trajectory( cluster.channelID().strip(), cluster.interStripFraction(), dxDy, dzDy, xAtYEq0, zAtYEq0,
                           yBegin, yEnd );
      float cos   = aSector->cosAngle();
      float error = aSector->pitch() / std::sqrt( 12.0 );

      if ( dzDy != 0 ) {
        throw GaudiException("dzDy is not zero", "UTHitHandler", StatusCode::FAILURE);
      }

      m_hits.addHit( std::forward_as_tuple( cluster, dxDy, xAtYEq0, zAtYEq0, yBegin, yEnd, cos, error ), station,
                     layer );
    }

    inline const HitContainer& hits() const { return m_hits; }

    inline HitRange hits( unsigned int station, unsigned int layer ) const { return m_hits.range( station, layer ); }

    inline void sortByXAtYEq0()
    {
      // sort by x for each layer for each station
      for(auto station : { 0 , 1 }) {
        for(auto layer : { 0 , 1 }) {
          auto current_hits = m_hits.range_(station, layer);
          // stable sort to keep the increase order of lhcbids
          std::stable_sort(current_hits.first, current_hits.second,
                           [&] (auto& lhs, auto& rhs) {
                             return lhs.xAtYEq0() < rhs.xAtYEq0();
                           });
        }
      }
    }

    inline void setOffsets()
    {
      // Prepare the HitContainer
      m_hits.setOffsets();
    }

  private:
    HitContainer m_hits;
  };
}
#endif // PRKERNEL_UTHITHANDLER_H
